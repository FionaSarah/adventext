"""
I'm not sure what sort of best practices this violates but..
I want to just throw various values at the browser in Javascript that
then gets JSON'd in-place in the base.html template.

You do this by altering the js_registery dictionary on a request object
wherever you might have access to it. Any key/value in this will be
represented in javascript in an object called 'registry'.

Yeah I know there's a huge opportunity for XSS here but I'm just
gonna be careful about what I pass through.
"""

from django.middleware.csrf import get_token

class JavascriptRegistry(object):
    def process_request(self, request):
        request.js_registry = {}

    def process_view(self, request, view_func, view_args, view_kwargs):
        request.js_registry['url_name'] = request.resolver_match.url_name
        request.js_registry['base_url'] = "http://" + request.get_host()
        # I wonder if there's any implication in popping the CSRF token
        # directly into the page, on all pages...
        request.js_registry['csrf_token'] = get_token(request)
