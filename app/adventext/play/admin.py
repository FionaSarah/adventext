from django.contrib import admin

from .models import Quest
from .models import QuestEntry
from .models import QuestPuzzleStatus
from .models import PuzzlesCompleted

admin.site.register(Quest)
admin.site.register(QuestEntry)
admin.site.register(QuestPuzzleStatus)
admin.site.register(PuzzlesCompleted)
