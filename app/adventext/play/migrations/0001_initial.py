# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('puzzle', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PuzzlesCompleted',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('rating', models.FloatField()),
                ('puzzle', models.ForeignKey(to='puzzle.Puzzle')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Quest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('quest_type', models.CharField(max_length=16, choices=[('guest', 'Guest'), ('quest', 'Quest'), ('build', 'Build'), ('approve', 'Approve')])),
                ('difficulty', models.CharField(max_length=16, choices=[('easy', 'Easy'), ('medium', 'Medium'), ('hard', 'Hard')])),
                ('is_broken', models.BooleanField(default=0)),
                ('is_finished', models.BooleanField(default=0)),
                ('user_message', models.TextField()),
                ('message_history', models.TextField()),
                ('current_puzzle', models.ForeignKey(null=True, to='puzzle.Puzzle', blank=True)),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='QuestEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('puzzle', models.ForeignKey(to='puzzle.Puzzle')),
                ('quest', models.ForeignKey(to='play.Quest')),
            ],
        ),
        migrations.CreateModel(
            name='QuestPuzzleStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('action_type', models.CharField(max_length=255)),
                ('param1', models.TextField()),
                ('param2', models.TextField()),
                ('param3', models.TextField()),
                ('param4', models.TextField()),
                ('param5', models.TextField()),
                ('quest', models.ForeignKey(to='play.Quest')),
            ],
        ),
    ]
