# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('play', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='puzzlescompleted',
            options={'verbose_name_plural': 'puzzles completed'},
        ),
        migrations.AlterModelOptions(
            name='questentry',
            options={'verbose_name_plural': 'quest entries'},
        ),
        migrations.AlterModelOptions(
            name='questpuzzlestatus',
            options={'verbose_name_plural': 'quest puzzle statuses'},
        ),
    ]
