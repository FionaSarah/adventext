# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('play', '0002_auto_20151025_1554'),
    ]

    operations = [
        migrations.AddField(
            model_name='questpuzzlestatus',
            name='key',
            field=models.TextField(default=''),
        ),
    ]
