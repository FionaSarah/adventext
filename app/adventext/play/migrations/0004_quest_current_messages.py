# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('play', '0003_questpuzzlestatus_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='quest',
            name='current_messages',
            field=jsonfield.fields.JSONField(default=dict),
        ),
    ]
