# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('play', '0004_quest_current_messages'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quest',
            name='message_history',
        ),
    ]
