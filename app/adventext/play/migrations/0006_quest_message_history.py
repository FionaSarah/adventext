# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('play', '0005_remove_quest_message_history'),
    ]

    operations = [
        migrations.AddField(
            model_name='quest',
            name='message_history',
            field=jsonfield.fields.JSONField(default=dict),
        ),
    ]
