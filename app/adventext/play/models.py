from django.db import models
from adventext.puzzle.models import Puzzle
from adventext.users.models import User
from jsonfield import JSONField


class Quest(models.Model):
    """
    Quests are what link puzzles together that players go on.
    """
    difficulty_choices = (
          ('easy', 'Easy'),
          ('medium', 'Medium'),
          ('hard', 'Hard'),
        )
    quest_type_choices = (
          ('guest', 'Guest'),
          ('quest', 'Quest'),
          ('build', 'Build'),
          ('approve', 'Approve'),
        )

    user = models.ForeignKey(User, blank = True, null = True)
    quest_type = models.CharField(max_length = 16, choices = quest_type_choices)
    current_puzzle = models.ForeignKey(Puzzle, blank = True, null = True)
    difficulty = models.CharField(max_length = 16, choices = difficulty_choices)
    is_broken = models.BooleanField(default = 0)
    is_finished = models.BooleanField(default = 0)
    user_message = models.TextField()
    current_messages = JSONField()
    message_history = JSONField()


class QuestEntry(models.Model):
    """
    Each entry represents a puzzle that has been played
    on a particular quest.
    """
    class Meta:
        verbose_name_plural = "quest entries"

    quest = models.ForeignKey(Quest)
    puzzle = models.ForeignKey(Puzzle)


class QuestPuzzleStatus(models.Model):
    """
    This keeps track of state within a puzzle.
    Each action represents something to change from the
    base puzzle state with params telling it what to change.
    """
    class Meta:
        verbose_name_plural = "quest puzzle statuses"

    quest = models.ForeignKey(Quest)
    action_type = models.CharField(max_length = 255)
    key = models.TextField(default = "")
    param1 = models.TextField()
    param2 = models.TextField()
    param3 = models.TextField()
    param4 = models.TextField()
    param5 = models.TextField()



class PuzzlesCompleted(models.Model):
    """
    Every time a puzzle is played we put it here
    so players don't get repeated puzzles in quests.
    """
    class Meta:
        verbose_name_plural = "puzzles completed"

    user = models.ForeignKey(User)
    puzzle = models.ForeignKey(Puzzle)
    rating = models.FloatField()
