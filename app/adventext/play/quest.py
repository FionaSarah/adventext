import random
from collections import OrderedDict
from django.conf import settings
from .models import Quest as QuestModel
from .models import QuestPuzzleStatus
from ..puzzle.models import Puzzle as PuzzleModel
from ..puzzle.models import RoomDirection as RoomDirectionModel
from ..puzzle.models import Room as RoomModel
from ..puzzle.models import Item as ItemModel
from ..puzzle.models import Interaction as InteractionModel


class QuestBuildException(Exception):
    """Thrown when the PuzzlePlayer object tries to create
    itself from the database."""
    pass

class PuzzlePlayerValidationException(Exception):
    """Thrown when the doing a turn and an error occurs that is
    recoverable, for instance if an action doesn't validate properly."""
    pass

class PuzzlePlayerException(Exception):
    """Thrown when the PuzzlePlayer object has an error when attempting
    to do something not related to building or doing a turn."""
    pass

class ControlFlowException(Exception):
    """Used to escape from a try/catch early"""
    pass

class PuzzleCompletedException(Exception):
    """Thrown during do_turn when a puzzle has been completed."""
    pass


class PuzzlePlayer:
    """
    A PuzzlePlayer object is a representation of a puzzle that can
    be polled for it's current state and interacted with.

    -- INITIALISING --

    It needs to be initialised with a companion quest object, if the
    puzzle is currently being played in that quest the state of the puzzle
    will be automatically updated to reflect it.
    If QuestBuildException is thrown when PuzzlePlayer is being initialised
    then there was an issue getting the state of the quest and it should not be used.

    -- DOING A TURN --

    If an action is to be done then the do_turn method is used.

    The first parameter to do_turn is the interaction to do, one of 'look', 'use',
    'pickup', 'talk', 'travel' or in the case of a custom interaction it's primary key
    as an int.
    If you're using an item in your inventory on something else use 'inventory:0'
    where 0 is the primary key on the item itself.

    The second parameter to do_turn is what do the interaction on.
    If travelling it should be the primary key on the room.
    If an item in the current room it should be a string containing 'room:0'
    where 0 is the primary key of the item. 'room:-1' denotes the room itself.
    If an item in the players inventory it should be 'inventory:0' where
    0 is the primary key of the item itself.

    If PuzzlePlayerException is thrown when calling do_turn then there was an
    unrecoverable issue and the quest should not be continued with.
    If PuzzlePlayerValidationException is thrown then there was a minor error that
    is recoverable from and the quest can safely be continued.
    if PuzzleCompletedException is called then the puzzle in this quest is finished
    do_turn should not be called anymore.

    -- GETTING MESSAGES --

    Once a turn is optionally made you can safely access 'messages' on PuzzlePlayer,
    it is a list of strings that were displayed on the most recent action.

    A history of recent messages is accessed via the 'history' member, also a list
    of strings.

    -- EXTRA METHODS --

    In addition to accessing messages, the following methods can be used once a turn
    is done, they each have docstrings describing their useage.

    get_directions_for_room
    get_current_player_directions
    get_current_player_interactions
    get_items_for_room
    get_items_for_current_room
    get_items_in_inventory
    get_current_room
    """

    # Example structure for a puzzle. Here just for reference, it
    # is rebuilt from scratch by the puzzle player
    puzzle = {
        'info' : {
            'pk' : 0,
            'name' : "Example",
            'author_pk' : 0,
            'author_name' : "Fiona",
            'difficulty' : "easy",
            'entrance_text' : "You entered the room.",
            'solution' : "Git gud.",
            'approved' : True
        },

        'global' : {
            'turn_num' : 0,
        },

        'player' : {
            'room' : 0,
            'inventory' : [1],
        },

        'rooms' : {
            0 : {
                'name' : "House",
                'description' : "It's a big house.",
                'initial_message' : "You accidently walk on a crisp packet.",
                'num_times_entered' : 0,
                'directions' : {
                  1:{
                    'name' : "Door",
                    'destination_pk' : None,
                    'is_exit' : True,
                    'visible' : True,
                    'travel_message' : "You fall through a paper door."
                  },
                },
                'items' : [0]
            },
        },

        'items' : {
            0 : {
                'name' : "Bucket",
                'description' : "It's a tiny red bucket.",
                'is_exit' : False,
                'can_pickup' : True,
                'visible' : True
            },
            1 : {
                'name' : "Water Pistal",
                'description' : "Cute and colourful. It's empty.",
                'is_exit' : False,
                'can_pickup' : False,
                'visible' : True
            },
        },

        'interactions' : {
            'look' : {
                'name' : "Look",
                'visible' : True,
                'fail_message' : None
            },
            'use' : {
                'name' : "Use",
                'visible' : True,
                'fail_message' : None
            },
            'pickup' : {
                'name' : "Pick Up",
                'visible' : True,
                'fail_message' : None
            },
            'talk' : {
                'name' : "Talk To",
                'visible' : True,
                'fail_message' : None
            },
            555 : {
                'name' : "Burp On",
                'visible' : True,
                'fail_message' : "You can't seem to summon a decent enough burp."
            },
        },
    }

    # Quest object associated with the puzzle being played
    quest = None

    # List of recent strings that have been shown to the player while playing
    history = []

    # List of new strings to be shown to the player as a result of an action
    # taken in the last turn.
    messages = []

    # constant used for interacting with room
    CURRENT_ROOM = -1

    def __init__(self, starting_puzzle, quest_object, puzzle_or_pk):
        """Initialise from a quest and puzzle's pk or query object. It will hit the
        database getting all information for the quest."""
        if not isinstance(quest_object, QuestModel):
            raise QuestBuildException("Passed quest is not a Quest queryset")

        self.starting_puzzle = starting_puzzle
        self.quest = quest_object
        self.puzzle = {}

        # Init global state
        self.puzzle['global'] = {
            'turn_num' : 1,
            }

        # Get puzzle
        try:
            puzzle_or_pk = int(puzzle_or_pk)
            puzzle = PuzzleModel.objects.get(pk = puzzle_or_pk)
        except (TypeError, ValueError):
            if not isinstance(puzzle_or_pk, PuzzleModel):
                raise QuestBuildException("Passed puzzle is not a Puzzle queryset")
            puzzle = puzzle_or_pk
        except PuzzleModel.DoesNotExist:
            raise QuestBuildException("Puzzle could not be found for building.")
        self.puzzle['info'] = {
            'pk' : puzzle.pk,
            'name' : puzzle.name,
            'author_pk' : puzzle.author.pk,
            'author_name' : puzzle.author.name,
            'difficulty' : puzzle.difficulty,
            'entrance_text' : puzzle.entrance_text,
            'solution' : puzzle.solution,
            'approved' : puzzle.approved
            }

        # Get rooms
        self.puzzle['rooms'] = {}
        all_rooms = RoomModel.objects.filter(puzzle = puzzle)
        if not len(all_rooms):
            raise QuestBuildException("No rooms found in puzzle when building.")
        for room in all_rooms:
            self.puzzle['rooms'][room.pk] = {
                'name' : room.name,
                'description' : room.description,
                'initial_message' : room.initial_message,
                'num_times_entered' : 0,
                'directions' : {},
                'items' : []
                }

        # Check start room exists
        if not puzzle.start_room.pk in self.puzzle['rooms']:
            raise QuestBuildException("Start room in puzzle does not exist.")

        # Get directions
        for direction in RoomDirectionModel.objects.filter(puzzle = puzzle):
            self.puzzle['rooms'][direction.room.pk]['directions'][direction.pk] = {
                'name' : direction.name,
                'destination_pk' : direction.destination.pk if direction.destination else None,
                'is_exit' : direction.is_exit,
                'visible' : direction.initially_visible,
                'travel_message' : direction.travel_message
                }

        # Init player state
        self.puzzle['player'] = {
            'room' : puzzle.start_room.pk,
            'inventory' : []
            }

        # Get items
        self.puzzle['items'] = {}
        for item in ItemModel.objects.filter(puzzle = puzzle):
            self.puzzle['items'][item.pk] = {
                'name' : item.name,
                'description' : item.description,
                'is_exit' : item.is_exit,
                'can_pickup' : item.can_pickup,
                'visible' : item.visible
                }
            if item.location > 0:
                if not item.location in self.puzzle['rooms']:
                    raise QuestBuildException("Item room location does not exist.")
                self.puzzle['rooms'][item.location]['items'].append(item.pk)
            elif item.location == -1:
                self.puzzle['player']['inventory'].append(item.pk)

        # Get interactions
        self.puzzle['interactions'] = {}
        for interaction in InteractionModel.objects.filter(puzzle=puzzle):
            pk = interaction.built_in_short_name if interaction.built_in else interaction.pk
            self.puzzle['interactions'][pk] = {
                'name' : interaction.name,
                'visible' : interaction.initially_visible,
                'fail_message' : None if interaction.built_in else interaction.fail_message
                }

        self.messages = list(self.quest.current_messages)
        self.history = list(self.quest.message_history)

        # Update with current status from database
        for state in QuestPuzzleStatus.objects.filter(quest=self.quest):
            self.set_status(state.action_type, state.key, state.param1, state.param2,
                            state.param3, state.param4, state.param5,
                            update_quest=False)

        # If first time in puzzle show the entrance text and enter the room
        if self.starting_puzzle:
            self.show_message(self.puzzle['info']['entrance_text'])
            self.do_enter_room()
            self.save_quest()

    def save_quest(self):
        self.quest.current_messages = self.messages
        self.quest.save()

    def do_turn(self, action=None, action_on=None):
        # Early escape if puzzle is finished
        if self.quest.is_finished:
            raise PuzzlePlayerValidationException("Puzzle has been completed.")
        # Check actions are valid
        if action is None:
            raise PuzzlePlayerValidationException("No action given.")
        if action == "travel":
            if action_on is None:
                raise PuzzlePlayerValidationException("No direction given to travel.")
        else:
            try:
                action = int(action)
            except ValueError:
                pass
            interactions = self.get_current_player_interactions()
            if not action in list(interactions.keys()) and not (isinstance(action, str) and action[:10] == "inventory:"):
                raise PuzzlePlayerValidationException("Invalid interaction passed.")
            if action_on is None:
                raise PuzzlePlayerValidationException("No item to do the interaction on given.")
        # Update history with what's currently there since we're going to show
        # a brand new set of messages
        self.quest.message_history = list(self.history + self.messages)[-settings.HISTORY_LENGTH:]
        self.history = list(self.quest.message_history)
        self.messages = []
        # Increment the turn count
        self.set_status("turn_num", "", int(self.puzzle['global']['turn_num']) + 1)
        # Do the specified action
        try:
            # ------------
            # Travelling
            # ------------
            if action == "travel":
                self.travel_using_direction(action_on)
                raise ControlFlowException()

            interaction = self.get_interaction_from_raw_action(action, action_on)

            # ------------
            # Pickup
            # ------------
            if interaction[0] == "pickup":
                if interaction[2] == PuzzlePlayer.CURRENT_ROOM:
                    self.show_default_builtin_interaction_message("pickup", self.get_current_room()['name'])
                else:
                    self.pickup_item(interaction[2])
                raise ControlFlowException()

            # ------------
            # Look
            # ------------
            if interaction[0] == "look":
                if interaction[2] == PuzzlePlayer.CURRENT_ROOM:
                    self.show_message(self.get_current_room()['description'])
                else:
                    self.look_at_item(interaction[2])
                raise ControlFlowException()

            # ------------
            # Talk
            # ------------
            if interaction[0] == "talk":
                if interaction[2] == PuzzlePlayer.CURRENT_ROOM:
                    self.show_default_builtin_interaction_message("talk", self.get_current_room()['name'])
                else:
                    self.talk_to_item(interaction[2])
                raise ControlFlowException()

            # ------------
            # Use
            # ------------
            if interaction[0] == "use":
                if interaction[2] == PuzzlePlayer.CURRENT_ROOM:
                    self.show_default_builtin_interaction_message("use", self.get_current_room()['name'])
                else:
                    self.use_item(interaction[2])
                raise ControlFlowException()

            # ------------
            # Custom interactions
            # ------------
            int_interaction = 0
            try:
                int_interaction = int(interaction[0])
            except (ValueError, TypeError):
                pass
            else:
                if interaction[2] == PuzzlePlayer.CURRENT_ROOM:
                    item_name = self.get_current_room()['name']
                else:
                    if not self.check_item_pk_visibility(int(interaction[2])):
                        raise PuzzlePlayerValidationException("Item attempted to apply interaction to is not accessible.")
                    item_name = self.puzzle['items'][int(interaction[2])]['name']
                self.show_custom_interaction_fail_message(int_interaction, item_name)
                raise ControlFlowException()

            # ------------
            # Use inventory item
            # ------------
            if interaction[0][:10] == "inventory:":
                if interaction[2] == PuzzlePlayer.CURRENT_ROOM:
                    self.show_default_builtin_interaction_message("inventory", self.get_current_room()['name'])
                else:
                    # Get the PK from the input string in the format "inventory:1" where 1 is the PK of the item.
                    action_split = interaction[0].split(":")
                    if not len(action_split) == 2 and not action_split[0] == "inventory":
                        raise PuzzlePlayerValidationException("Item passed to use format invalid.")
                    self.use_item_on(action_split[1], interaction[2])
                raise ControlFlowException()

        except PuzzlePlayerValidationException:
            raise
        except ControlFlowException:
            pass
        except PuzzleCompletedException:
            self.quest.is_finished = True
        finally:
            # Save messages for showing
            self.save_quest()

    def get_interaction_from_raw_action(self, action, action_on):
        """Returns a three item tuple parsed from strings that typically come
        from POSTed values.
        action is either a string representing a standard interaction or an int
        representing a custom interaction.
        action_on is a string containing two parts separated by a colon. The first
        part is always either 'room' or 'inventory', denoting the location of the
        item that an action is being applied to. The second part is a number that's
        the pk of the item in question. -1 is the room itself.
        The returned tuple is (type, location, item). Type being the short string,
        location being either 'room' or 'inventory' and item being an int.
        Throws PuzzlePlayerValidation if any of the params are wrongly formatted or the
        action type does not exist."""
        # Get action PK or short name
        try:
            action = int(action)
        except (ValueError, TypeError):
            if not isinstance(action, str):
                raise PuzzlePlayerValidationException("Action passed was not a string or number.")
        # Check action exists only for non inventory actions
        if not (isinstance(action, str) and action[:10] == "inventory:"):
            if not action in self.puzzle['interactions']:
                raise PuzzlePlayerValidationException("Interaction passed does not exist in puzzle.")
        # Get action on value
        if not isinstance(action_on, str):
            raise PuzzlePlayerValidationException("Action on passed was not a string.")
        action_on = action_on.split(":")
        if not len(action_on) == 2:
            raise PuzzlePlayerValidationException("Action item not passed correctly.")
        # Check doing action on valid place
        if not action_on[0] in ["room", "inventory"]:
            raise PuzzlePlayerValidationException("Action item location invalid.")
        # Get action on item
        try:
            action_on_item = int(action_on[1])
        except ValueError:
            raise PuzzlePlayerValidationException("Action item not an int.")
        # Send back parsed interaction
        return (action, action_on[0], int(action_on[1]))

    def do_enter_room(self):
        player_loc = self.puzzle['player']['room']
        # Show initial message
        if not self.puzzle['rooms'][player_loc]['num_times_entered']:
            self.show_message(self.puzzle['rooms'][player_loc]['initial_message'])
        # display description
        self.show_message(self.puzzle['rooms'][player_loc]['description'])
        # increment num times entered
        self.set_status(
            "room_num_times_entered",
            player_loc,
            int(self.puzzle['rooms'][player_loc]['num_times_entered']) + 1
            )

    def travel_using_direction(self, direction_to_traverse):
        """Shortcut method for moving the player to another room."""
        # Make sure the direction we're using is viable
        accessible_dirs = self.get_current_player_directions()
        if not direction_to_traverse in accessible_dirs:
            raise PuzzlePlayerValidationException("Attempted to travel to room that is not accessible.")
        direction = accessible_dirs[int(direction_to_traverse)]
        # Display the travelling message
        if direction['travel_message'].strip():
            self.show_message(direction['travel_message'].strip())
        else:
            self.show_message("You travel {}".format(direction['name']))
        # If the puzzle is an exit we throw the relevant exception which is caught
        # by do_turn
        if direction['is_exit']:
            raise PuzzleCompletedException()
        # Set the player to the new loc
        self.set_status(
            "player_set_location",
            "",
            int(direction['destination_pk'])
            )
        # Trigger us entering the room we're in, doing all the required
        # descriptions
        self.do_enter_room()

    def check_item_pk_visibility(self, item_pk):
        """Check if an item is available by PK. Returns boolean."""
        if not item_pk in self.puzzle['items']:
            return False
        if not item_pk in self.puzzle['player']['inventory']:
            item_list = self.get_items_for_current_room()
            if not item_pk in item_list:
                return False
        else:
            if not self.puzzle['items'][item_pk]['visible']:
                return False
        return True

    def pickup_item(self, item_pk):
        """Shortcut method for picking up an item"""
        if not self.check_item_pk_visibility(item_pk):
            raise PuzzlePlayerValidationException("Item attempted to pick up is not accessible.")
        item = self.puzzle['items'][item_pk]
        if item_pk in self.puzzle['player']['inventory']:
            self.show_message("You're already holding the {}.".format(item['name']))
            return
        if not item['can_pickup']:
            self.show_default_builtin_interaction_message("pickup", item['name'])
            return
        self.show_message("You take the {}.".format(item['name']))
        self.set_status(
            "item_set_location",
            item_pk,
            -1
            )

    def look_at_item(self, item_pk):
        """Shortcut method for looking at an item"""
        if not self.check_item_pk_visibility(item_pk):
            raise PuzzlePlayerValidationException("Item attempted to look at is not accessible.")
        self.show_message(self.puzzle['items'][item_pk]['description'])

    def use_item(self, item_pk):
        """Shortcut method for using an item"""
        if not self.check_item_pk_visibility(item_pk):
            raise PuzzlePlayerValidationException("Item attempted to use is not accessible.")
        if self.puzzle['items'][item_pk]['is_exit']:
            raise PuzzleCompletedException()
        self.show_default_builtin_interaction_message("use", self.puzzle['items'][item_pk]['name'])

    def use_item_on(self, item_pk, item_on_pk):
        """Shortcut method for using an item on something else"""
        try:
            item_pk = int(item_pk)
            item_on_pk = int(item_on_pk)
        except ValueError:
            raise PuzzlePlayerValidationException("One of the items passed were not integers")
        # Check items exist
        if not item_pk in self.puzzle['items']:
            raise PuzzlePlayerValidationException("Item attempted to use is not in item list.")
        if not item_on_pk in self.puzzle['items']:
            raise PuzzlePlayerValidationException("Item attempted to use on is not in item list.")
        # Check item we're using is in our hand
        if not item_pk in self.puzzle['player']['inventory']:
            raise PuzzlePlayerValidationException("Item attempted to use on is not in inventory.")
        # Check we can see both items
        if not self.puzzle['items'][item_pk]['visible']:
            raise PuzzlePlayerValidationException("Item attempted to use is not visible.")
        if not self.puzzle['items'][item_on_pk]['visible']:
            raise PuzzlePlayerValidationException("Item attempted to use on is not visible.")
        # If not using item on something in inventory, check we can access it
        if not item_on_pk in self.puzzle['player']['inventory']:
            item_list = self.get_items_for_current_room()
            if not item_on_pk in item_list:
                raise PuzzlePlayerValidationException("Item attempted to use on is not accessible.")
        # Show relevant messages
        if item_pk == item_on_pk:
            self.show_message("You can't use something on itself.")
        else:
            self.show_default_builtin_interaction_message("inventory", self.puzzle['items'][item_pk]['name'])

    def talk_to_item(self, item_pk):
        """Shortcut method for talking to an item"""
        if not self.check_item_pk_visibility(item_pk):
            raise PuzzlePlayerValidationException("Item attempted to talk to is not accessible.")
        self.show_default_builtin_interaction_message("talk", self.puzzle['items'][item_pk]['name'])

    def set_status(self,  action_type, key="", param1="", param2="",
                   param3="", param4="", param5="", update_quest=True):
        if not hasattr(self, "action_"+action_type):
            raise PuzzlePlayerException("Action does not exist.")
        # Call the relevant action passing key and params
        getattr(self, "action_"+action_type)(
            str(key), str(param1), str(param2), str(param3),
            str(param4), str(param5)
            )
        # If we want to we save the status in the database
        if update_quest:
            QuestPuzzleStatus.objects.update_or_create(
                quest=self.quest,
                action_type=action_type,
                key=str(key),
                defaults={
                  'param1':str(param1),
                  'param2':str(param2),
                  'param3':str(param3),
                  'param4':str(param4),
                  'param5':str(param5),
                }
            )

    def show_message(self, text_to_show):
        self.messages.append(text_to_show)

    def get_directions_for_room(self, room_pk):
        """Gets the current available directions for the
        passed room pk. As a dictionary of dictionaries.
        Hidden directions are not included.
        Direction pks as keys and a dictionary for each
        one containing 'name', 'destination_pk', 'is_exit'
        and 'travel_message"""
        if not room_pk in self.puzzle['rooms']:
            raise PuzzlePlayerException("Room does not exist in puzzle")
        dirs = {}
        for dir_pk, dir_info in self.puzzle['rooms'][room_pk]['directions'].items():
            if dir_info['visible']:
                dirs[dir_pk] = dir_info.copy()
        return dirs

    def get_current_player_directions(self):
        """Shortcut for getting the directions the player
        can use at the current time. Returns a dictionary of
        direction pk to info."""
        return self.get_directions_for_room(self.puzzle['player']['room'])

    def get_current_player_interactions(self):
        """Shortcut for getting the actions the player
        can do at the current time. Returns a dictionary of
        interaction pk to info."""
        interactions = {}
        for int_pk, int_info in self.puzzle['interactions'].items():
            if int_info['visible']:
                interactions[int_pk] = int_info.copy()
        return interactions

    def get_items_for_room(self, room_pk):
        """Returns a dictionary of dictionaries containing the
        items that are available in the given room.
        Items not visible are not included.
        Item pks as keys and a dictionary for each item containing
        'name', 'description','visible','is_exit','can_pickup'"""
        items = {}
        for item_pk in self.puzzle['rooms'][room_pk]['items']:
            item_info = self.puzzle['items'][item_pk]
            if item_info['visible']:
                items[item_pk] = item_info.copy()
        return items

    def get_items_for_current_room(self):
        """Shortcut for getting the items the player
        can see in the room  at the current time. Returns
        a dictionary of item pk to info."""
        return self.get_items_for_room(self.puzzle['player']['room'])

    def get_items_in_inventory(self):
        """Returns a dictionary of dictionaries containing the
        items that are in the players inventory. Items not visible
        are not included. Item pks are keys and a dictionary for each
        item like get_items_for_room()"""
        items = {}
        for item_pk in self.puzzle['player']['inventory']:
            item_info = self.puzzle['items'][item_pk]
            if item_info['visible']:
                items[item_pk] = item_info.copy()
        return items

    def get_current_room(self):
        """Returns a dictionary containing information for the current
        room that the player is in."""
        return self.puzzle['rooms'][self.puzzle['player']['room']]

    default_builtin_interaction_messages = {
        "look" : [
			"There's nothing special about the {}.",
			"Nice {}.",
			"It's just a regular {}"
            ],
        "use" : [
			"That doesn't work.",
			"It doesn't do anything.",
			"Nothing special happens."
            ],
        "pickup" : [
			"You can't pick that up.",
			"The {} can't be picked up."
			],
        "inventory" : [
			"You can't use it on that.",
			"It doesn't work with that.",
			"They can't be combined.",
			"Nothing interesting happens.",
			"They don't do anything together."
			],
        "talk" : [
            "The {} doesn't talk back.",
            "It's not much for conversation.",
            "You feel like you're talking to yourself."
            ],
		}

    def show_default_builtin_interaction_message(self, interaction_type,  item_name):
        """Shows the default interaction message for a failed interaction.
        Pass in the short name for the interaction and the name of the item
        it's being done on for message replacement."""
        message = random.choice(self.default_builtin_interaction_messages[interaction_type])
        self.show_message(message.format(item_name))

    def show_custom_interaction_fail_message(self, interaction_pk, item_name):
        """Shows the fail message for custom interactions. Pass the PK of the interaction and
        the name of item it's being done on for message replacement."""
        if not interaction_pk in self.puzzle['interactions']:
            raise PuzzlePlayerValidationException("Interaction passed does not exist.")
        message = self.puzzle['interactions'][interaction_pk]['fail_message']
        self.show_message(message.format(item_name))


    ###############################################################
    #### ACTIONS
    ##############################################################

    """
    Each action method represent as an action taken on a puzzle.
    The puzzle member is manipulated to apply the action.
    Each action is passed an optional key and up to 5 parameters.
    The key is used if a state can be applied to a number of items.
    """
    # These actions are not usable by the puzzle builder and are
    # used to save some unmanipulatable state variables.
    internal_actions = ["turn_num", "room_num_times_entered"]

    def action_turn_num(self, key, turn_to_set, *ignore):
        """INTERNAL: Used to keep track of what turn # the player is on"""
        try:
            self.puzzle['global']['turn_num'] = int(turn_to_set)
        except ValueError:
            raise PuzzlePlayerException("Turn num was not a number")

    def action_room_num_times_entered(self, room_pk, num_times_entered, *ignore):
        """INTERNAL: Used to keep track of how many the player has entered
        a room."""
        if not int(room_pk) in self.puzzle['rooms']:
            raise PuzzlePlayerException("Attempted to set entered count of room that does not exist.")
        try:
            self.puzzle['rooms'][int(room_pk)]['num_times_entered'] = int(num_times_entered)
        except ValueError:
            raise PuzzlePlayerException("Times entered was not a number")

    def action_player_set_location(self, key, room_pk, *ignore):
        """Sets which room the player is in."""
        if not int(room_pk) in self.puzzle['rooms']:
            raise PuzzlePlayerException("Attempted to travel to room that does not exist.")
        self.puzzle['player']['room'] = int(room_pk)

    def action_item_set_location(self, item_pk, location_pk, *ignore):
        """Sets where an item is. -1 is the player inventory."""
        # get vals
        try:
            item_pk = int(item_pk)
            location_pk = int(location_pk)
        except (ValueError, TypeError):
            raise PuzzlePlayerException("Item or location passed was not a number.")
        # Check item exists
        if not item_pk in self.puzzle['items']:
            raise PuzzlePlayerException("Attempted to pick up item that does not exist")
        # Check room exists
        if not location_pk == 0 and not location_pk == -1 and not location_pk in self.puzzle['rooms']:
            raise PuzzlePlayerException("Attempted to place item in room that does not exist")
        # Remove from any room lists
        for room_pk in self.puzzle['rooms']:
            try:
                self.puzzle['rooms'][room_pk]['items'].remove(item_pk)
            except ValueError:
                pass
        # remove from inventory
        try:
            self.puzzle['player']['inventory'].remove(item_pk)
        except ValueError:
            pass
        # Add to inventory or room
        if int(location_pk) == -1:
            self.puzzle['player']['inventory'].append(item_pk)
        elif int(location_pk) > 0:
            self.puzzle['rooms'][location_pk]['items'].append(item_pk)

    def action_interaction_set_visibility(self, interaction_id, is_visible, *ignore):
        """Sets if the player can see and use the interaction passed. It's ethier a short name
        indicating a built-in interaction or a pk indicating an custom interaction. Also pass
        a boolean for if it should or shouldn't be visible."""
        # Check if ID is valid
        pk = None
        if isinstance(interaction_id, str):
            pk = interaction_id
        else:
            try:
                pk = int(interaction_id)
            except (ValueError, TypeError):
                raise PuzzlePlayerException("Interaction ID passed was not a string or number.")
        # Check is_visible is a bool
        if not isinstance(is_visible, bool):
            raise PuzzlePlayerException("New value of interactions visibility was not a boolean.")
        # Check ID exists
        if not pk in self.puzzle['interactions']:
            raise PuzzlePlayerException("Attempted to set visibility of interaction that does not exist.")
        # Set visibility
        self.puzzle['interactions'][pk]['visible'] = is_visible


# A dictionary of triggers and associated info
event_triggers = OrderedDict([
    ("player_interaction", {
        'name' : "Player interaction",
        'description' : "If the player interacts with an item or room. This includes all default interactions as well as custom ones.",
        'category' : "global",
        'params' : OrderedDict([("interaction", "Player is doing..."), ("item_or_room", "Interacting with...")])
    }),
    ("player_item_combination", {
        'name' : "Player item combination",
        'description' : "If the player uses an item in their inventory with another item or room.",
        'category' : "global",
        'params' : OrderedDict([("item_using", "Item using..."), ("item_or_room", "Using on...")])
    }),
    ("player_travels", {
        'name' : "Player travels",
        'description' : "If the player travels along a particular direction.",
        'category' : "global",
        'params' : OrderedDict([("direction", "Direction")])
    }),
    ("turns_elapsed", {
        'name' : "Number of turns elapsed",
        'description' : "Will be true if the number of turns done in the puzzle equals or exceeds a particular number.<br />" \
            "A turn is defined as an interaction done by the player, even if that interaction did not result in anything happening.",
        'category' : "global",
        'params' : OrderedDict([("number", "Turns")])
    }),
    ("player_in_room", {
        'name' : "Player is in room",
        'description' : "If the player is currently in a particular room.",
        'category' : "global",
        'params' : OrderedDict([("room", "Room")])
    }),
    ("times_event_triggered", {
        'name' : "Number of times event was triggered",
        'description' : "Will be true if the number of times a particular event has been triggered equals or exceeds a particular number.<br />" \
            "An event is triggered when it's trigger criteria are met and it executes the actions assigned to it.<br />" \
            "<strong>Note:</strong> If you use the \"Reset event count\" action this trigger will compare against the reset count.",
        'category' : "global",
        'params' : OrderedDict([("event", "Event to check"), ("number", "Times event was triggered")])
    }),

    ("value_comparison", {
        'name' : "Check a value",
        'description' : "Used to check what a value currently is. First select a value, then any type of comparison.<br />" \
            "It is possible to comparue volues with others.",
        'category' : "values",
        'params' : OrderedDict([("value_comparison", "Value to check")])
    }),

    ("item_location", {
        'name' : "Item location",
        'description' : "The current location of an item. Including inventory.",
        'category' : "items",
        'params' : OrderedDict([("item", "Item"), ("item_or_rooms", "Location")])
    }),
    ("item_visible", {
        'name' : "Item is visible",
        'description' : "If an item is currently visible",
        'category' : "item",
        'params' : OrderedDict([("item", "Item")])
    }),

    ("interaction_visible", {
        'name' : "Interaction visible",
        'description' : "Check if an interaction is visible and can be used by the player.",
        'category' : "interactions",
        'params' : OrderedDict([("interaction", "Interaction")])
    }),
])

# List of user facing names for categories
event_trigger_categories = OrderedDict([
    ('global', "Global Triggers"),
    ('values', "Values"),
    ('items', "Items"),
    ('interactions', "Interactions")
])
