from django import template

register = template.Library()

@register.filter(name='comma_delimited_list_of_items')
def comma_delimited_list_of_items(item_list):
    item_names = []
    for pk,i in item_list.items():
        item_names.append(i['name'])
    return ", ".join(sorted(item_names))

@register.filter(name='get_by_key')
def get_by_key(var, key):
    return var[key]
