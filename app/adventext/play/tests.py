from django.test import TestCase
from adventext.users.models import User
from ..puzzle.models import Puzzle, Room, RoomDirection, Item, Interaction
from .models import Quest, QuestPuzzleStatus
from .quest import PuzzlePlayer
from .quest import QuestBuildException, PuzzlePlayerValidationException, PuzzlePlayerException, PuzzleCompletedException


class MethodWrapper:
   def __init__(self, method):
     self.method = method
     self.was_called = False

   def __call__(self, *args, **kwargs):
     self.method(*args, **kwargs)
     self.was_called = True


class BaseTestCase(TestCase):
    def setUp(self):
        # Create builder user
        self.user = User.objects.create_user(
            username="pikachu", email="pikachu@adventext.com", password="teamrocketsucks"
            )
        self.user.is_builder = True
        self.user.save()

        # Create puzzle
        self.puzzle = Puzzle(
            name="Test Puzzle",
            author=self.user,
            difficulty='easy',
            entrance_text="You enter a big room."
            )
        self.puzzle.save()

        # Create rooms
        room = Room(
            pk=1,
            puzzle=self.puzzle,
            name="Start",
            description="The start of the room",
            initial_message="You enter the start of the room"
            )
        room.save()
        self.puzzle.start_room = room
        self.puzzle.save()

        room2 = Room(
            pk=2,
            puzzle=self.puzzle,
            name="End",
            description="The end of the room",
            initial_message="You enter the end of the room"
            )
        room2.save()

        # Create directions
        roomdir = RoomDirection(
            pk=1,
            puzzle=self.puzzle,
            room=room,
            destination=room2,
            name="East",
            is_exit=False,
            initially_visible=True
            )
        roomdir.save()

        room2dir = RoomDirection(
            pk=2,
            puzzle=self.puzzle,
            room=room2,
            destination=room,
            name="West",
            is_exit=False,
            initially_visible=True
            )
        room2dir.save()

        room2exitdir = RoomDirection(
            pk=3,
            puzzle=self.puzzle,
            room=room2,
            destination=None,
            name="North",
            is_exit=True,
            initially_visible=True,
            travel_message="You run away"
            )
        room2exitdir.save()

        # Items
        item1 = Item(
            pk=1,
            puzzle=self.puzzle,
            name="Bucket",
            description="A crappy bucket.",
            is_exit=False,
            can_pickup=True,
            visible=True,
            location=room.pk
            )
        item1.save()

        item2 = Item(
            pk=2,
            puzzle=self.puzzle,
            name="Water Gun",
            description="A cute water gun.",
            is_exit=False,
            can_pickup=True,
            visible=True,
            location=-1
            )
        item2.save()

        item3 = Item(
            pk=3,
            puzzle=self.puzzle,
            name="Nothing",
            description="It's actually nothing.",
            is_exit=False,
            can_pickup=True,
            visible=False,
            location=0
            )
        item3.save()

        item4 = Item(
            pk=4,
            puzzle=self.puzzle,
            name="Door",
            description="A huge wooden door.",
            is_exit=True,
            can_pickup=True,
            visible=True,
            location=room.pk
            )
        item4.save()

        # Default interactions
        pk = 0
        for short_name, name in Interaction.built_in_choices:
            pk += 1
            i = Interaction(
                pk=pk,
                puzzle=self.puzzle,
                name=name,
                built_in_short_name=short_name,
                built_in=True,
                initially_visible=True,
                fail_message=""
                )
            i.save()

        # A custom interaction
        i = Interaction(
            pk=pk+1, #5
            puzzle=self.puzzle,
            name="Sarc At",
            built_in=False,
            initially_visible=True,
            fail_message="Your sarcasm falls flat."
            )
        i.save()

        # Create quest
        self.setup_quest()

        # Create puzzle player for room
        self.puzzle_player = PuzzlePlayer(self.starting_puzzle, self.quest, self.puzzle)
        self.reset_interaction_message_list()

    def setup_quest(self):
        self.quest = Quest(
            user=None,
            quest_type='guest',
            current_puzzle=self.puzzle
            )
        self.quest.save()
        self.starting_puzzle = True

    def reset_interaction_message_list(self):
        self.puzzle_player.default_builtin_interaction_messages = {
            "look": ["Nice {}."],
            "use": ["That doesn't work."],
            "pickup": ["You can't pick that up."],
            "inventory": ["They can't be combined."],
            "talk" : ["It's not talking."],
            }


class PuzzlePlayerIsInitialisedCorrectlyTest(BaseTestCase):

    def test_puzzle_is_loaded(self):
        self.assertEqual(self.puzzle_player.puzzle['info']['pk'], self.puzzle.pk)
        self.assertEqual(self.puzzle_player.puzzle['info']['name'], self.puzzle.name)
        self.assertEqual(self.puzzle_player.puzzle['info']['author_pk'], self.puzzle.author.pk)
        self.assertEqual(self.puzzle_player.puzzle['info']['author_name'], self.puzzle.author.name)
        self.assertEqual(self.puzzle_player.puzzle['info']['difficulty'], self.puzzle.difficulty)
        self.assertEqual(self.puzzle_player.puzzle['info']['entrance_text'], self.puzzle.entrance_text)
        self.assertEqual(self.puzzle_player.puzzle['info']['solution'], self.puzzle.solution)
        self.assertEqual(self.puzzle_player.puzzle['info']['approved'], self.puzzle.approved)

    def test_global_state_initialised(self):
        self.assertEqual(self.puzzle_player.puzzle['global']['turn_num'], 1)

    def test_rooms_loaded(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms']), 2)
        r = self.puzzle_player.puzzle['rooms'][1]
        self.assertEqual(r['name'], "Start")
        self.assertEqual(r['description'], "The start of the room")
        self.assertEqual(r['initial_message'], "You enter the start of the room")

        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertIn(4, self.puzzle_player.puzzle['rooms'][1]['items'])
        self.assertIn(1, self.puzzle_player.puzzle['rooms'][1]['items'])
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['items']), 0)

    def test_directions_loaded(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['directions']), 1)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['directions']), 2)
        dir1 = self.puzzle_player.puzzle['rooms'][2]['directions'][2]
        self.assertEqual(dir1['destination_pk'], 1)
        self.assertEqual(dir1['name'], "West")
        self.assertEqual(dir1['is_exit'], False)
        self.assertEqual(dir1['visible'], True)
        self.assertEqual(dir1['travel_message'], "")
        dir2 = self.puzzle_player.puzzle['rooms'][2]['directions'][3]
        self.assertEqual(dir2['destination_pk'], None)
        self.assertEqual(dir2['name'], "North")
        self.assertEqual(dir2['is_exit'], True)
        self.assertEqual(dir2['visible'], True)
        self.assertEqual(dir2['travel_message'], "You run away")

    def test_items_loaded(self):
        item1 = self.puzzle_player.puzzle['items'][1]
        self.assertEqual(item1['name'], "Bucket")
        self.assertEqual(item1['description'], "A crappy bucket.")
        self.assertEqual(item1['is_exit'], False)
        self.assertEqual(item1['can_pickup'], True)
        self.assertEqual(item1['visible'], True)

        item2 = self.puzzle_player.puzzle['items'][2]
        self.assertEqual(item2['name'], "Water Gun")
        self.assertEqual(item2['description'], "A cute water gun.")
        self.assertEqual(item2['is_exit'], False)
        self.assertEqual(item2['can_pickup'], True)
        self.assertEqual(item2['visible'], True)

        item3 = self.puzzle_player.puzzle['items'][3]
        self.assertEqual(item3['name'], "Nothing")
        self.assertEqual(item3['description'], "It's actually nothing.")
        self.assertEqual(item3['is_exit'], False)
        self.assertEqual(item3['can_pickup'], True)
        self.assertEqual(item3['visible'], False)

        item4 = self.puzzle_player.puzzle['items'][4]
        self.assertEqual(item4['name'], "Door")
        self.assertEqual(item4['description'], "A huge wooden door.")
        self.assertEqual(item4['is_exit'], True)
        self.assertEqual(item4['can_pickup'], True)
        self.assertEqual(item4['visible'], True)

    def test_interactions_loaded(self):
        i = self.puzzle_player.puzzle['interactions']['look']
        self.assertEqual(i['name'], "Look")
        self.assertEqual(i['visible'], True)
        self.assertEqual(i['fail_message'], None)
        i = self.puzzle_player.puzzle['interactions']['use']
        self.assertEqual(i['name'], "Use")
        self.assertEqual(i['visible'], True)
        self.assertEqual(i['fail_message'], None)
        i = self.puzzle_player.puzzle['interactions']['pickup']
        self.assertEqual(i['name'], "Pick Up")
        self.assertEqual(i['visible'], True)
        self.assertEqual(i['fail_message'], None)
        i = self.puzzle_player.puzzle['interactions']['talk']
        self.assertEqual(i['name'], "Talk To")
        self.assertEqual(i['visible'], True)
        self.assertEqual(i['fail_message'], None)
        i = self.puzzle_player.puzzle['interactions'][5]
        self.assertEqual(i['name'], "Sarc At")
        self.assertEqual(i['visible'], True)
        self.assertEqual(i['fail_message'], "Your sarcasm falls flat.")

    def global_state_is_initialised(self):
        self.assertEqual(self.puzzle_player.puzzle['global']['turn_num'], 0)

    def player_state_is_initialised(self):
        self.assertEqual(self.puzzle_player.puzzle['player']['room'], self.puzzle.start_room.pk)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 1)
        self.assertEqual(self.puzzle_player.puzzle['player']['inventory'][0], 2)

    def test_history_is_empty(self):
        self.assertEqual(len(self.puzzle_player.history), 0)

    def test_shown_initial_entrance_text(self):
        self.assertIn(self.puzzle.entrance_text, self.puzzle_player.messages)

    def test_shown_initial_room_text(self):
        self.assertIn("You enter the start of the room", self.puzzle_player.messages)

    def test_shown_initial_room_description(self):
        self.assertIn("The start of the room", self.puzzle_player.messages)

    def test_shown_only_initial_messages(self):
        self.assertEqual(len(self.puzzle_player.messages), 3)

    def test_num_times_entered_in_rooms(self):
        self.assertEqual(self.puzzle_player.puzzle['rooms'][1]['num_times_entered'], 1)
        self.assertEqual(self.puzzle_player.puzzle['rooms'][2]['num_times_entered'], 0)

    def test_initial_quest_state_was_created(self):
        states = QuestPuzzleStatus.objects.filter(quest=self.quest)
        self.assertEqual(len(states), 1)
        self.assertEqual(states[0].action_type, "room_num_times_entered")
        self.assertEqual(states[0].key, "1")
        self.assertEqual(states[0].param1, "1")
        self.assertEqual(states[0].param2, "")
        self.assertEqual(states[0].param3, "")
        self.assertEqual(states[0].param4, "")
        self.assertEqual(states[0].param5, "")

    def test_puzzle_passed_invalid_quest(self):
        with self.assertRaises(QuestBuildException):
            self.puzzle_player = PuzzlePlayer(self.starting_puzzle, self.quest, self.quest)

    def test_puzzle_passed_invalid_puzzle_id(self):
        with self.assertRaises(QuestBuildException):
            self.puzzle_player = PuzzlePlayer(self.starting_puzzle, self.quest, 10)

    def test_puzzle_passed_puzzle_id_nan(self):
        with self.assertRaises(QuestBuildException):
            self.puzzle_player = PuzzlePlayer(self.starting_puzzle, self.quest, "puzzle")

    def test_puzzle_passed_no_rooms(self):
        puzzle2 = Puzzle(name="Test", author=self.user, difficulty='easy')
        puzzle2.save()
        with self.assertRaises(QuestBuildException):
            self.puzzle_player = PuzzlePlayer(self.starting_puzzle, self.quest, puzzle2)


class PuzzlePlayerMethodsTest(BaseTestCase):

    def test_show_message(self):
        self.puzzle_player.show_message("Testing message")
        self.assertIn("Testing message", self.puzzle_player.messages)

    def test_save_quest(self):
        self.puzzle_player.show_message("Testing message")
        self.puzzle_player.save_quest()
        self.assertIn("Testing message", self.quest.current_messages)

    def test_get_directions_for_room(self):
        dirs = self.puzzle_player.get_directions_for_room(2)
        self.assertEqual(len(dirs), 2)
        self.assertEqual(dirs[3]['destination_pk'], None)
        self.assertEqual(dirs[3]['name'], "North")
        self.assertEqual(dirs[3]['is_exit'], True)
        self.assertEqual(dirs[3]['visible'], True)
        self.assertEqual(dirs[3]['travel_message'], "You run away")

    def test_get_directions_for_room_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.get_directions_for_room(3)

    def test_get_current_player_directions(self):
        dirs = self.puzzle_player.get_current_player_directions()
        self.assertEqual(len(dirs), 1)
        self.assertEqual(dirs[1]['destination_pk'], 2)
        self.assertEqual(dirs[1]['name'], "East")
        self.assertEqual(dirs[1]['is_exit'], False)
        self.assertEqual(dirs[1]['visible'], True)
        self.assertEqual(dirs[1]['travel_message'], "")

    def test_get_current_player_directions_after_travelling(self):
        self.puzzle_player.travel_using_direction(1)
        dirs = self.puzzle_player.get_current_player_directions()
        self.assertEqual(len(dirs), 2)
        self.assertEqual(dirs[2]['destination_pk'], 1)
        self.assertEqual(dirs[2]['name'], "West")
        self.assertEqual(dirs[2]['is_exit'], False)
        self.assertEqual(dirs[2]['visible'], True)
        self.assertEqual(dirs[2]['travel_message'], "")

    def test_travel_puzzle_exit_completes_puzzle(self):
        self.puzzle_player.travel_using_direction(1)
        with self.assertRaises(PuzzleCompletedException):
            self.puzzle_player.travel_using_direction(3)

    def test_travel_puzzle_exit_shows_travel_message(self):
        self.puzzle_player.travel_using_direction(1)
        try:
            self.puzzle_player.travel_using_direction(3)
        except PuzzleCompletedException:
            pass
        self.assertIn("You run away", self.puzzle_player.messages)

    def test_get_items_for_room(self):
        items = self.puzzle_player.get_items_for_room(1)
        self.assertEqual(len(items), 2)
        self.assertEqual(items[1]['name'], "Bucket")
        self.assertEqual(items[1]['description'], "A crappy bucket.")
        self.assertEqual(items[1]['is_exit'], False)
        self.assertEqual(items[1]['can_pickup'], True)
        self.assertEqual(items[1]['visible'], True)
        self.assertEqual(items[4]['name'], "Door")
        self.assertEqual(items[4]['description'], "A huge wooden door.")
        self.assertEqual(items[4]['is_exit'], True)
        self.assertEqual(items[4]['can_pickup'], True)
        self.assertEqual(items[4]['visible'], True)

    def test_get_items_for_current_room(self):
        items = self.puzzle_player.get_items_for_current_room()
        self.assertEqual(len(items), 2)
        self.assertEqual(items[1]['name'], "Bucket")
        self.assertEqual(items[1]['description'], "A crappy bucket.")
        self.assertEqual(items[1]['is_exit'], False)
        self.assertEqual(items[1]['can_pickup'], True)
        self.assertEqual(items[1]['visible'], True)
        self.assertEqual(items[4]['name'], "Door")
        self.assertEqual(items[4]['description'], "A huge wooden door.")
        self.assertEqual(items[4]['is_exit'], True)
        self.assertEqual(items[4]['can_pickup'], True)
        self.assertEqual(items[4]['visible'], True)

    def test_get_items_for_current_room_after_travelling(self):
        self.puzzle_player.travel_using_direction(1)
        items = self.puzzle_player.get_items_for_current_room()
        self.assertEqual(len(items), 0)

    def test_get_items_in_inventory(self):
        items = self.puzzle_player.get_items_in_inventory()
        self.assertEqual(len(items), 1)
        self.assertEqual(items[2]['name'], "Water Gun")
        self.assertEqual(items[2]['description'], "A cute water gun.")
        self.assertEqual(items[2]['is_exit'], False)
        self.assertEqual(items[2]['can_pickup'], True)
        self.assertEqual(items[2]['visible'], True)

    def test_get_current_player_interactions(self):
        interactions = self.puzzle_player.get_current_player_interactions()
        self.assertEqual(len(interactions), 5)
        self.assertEqual(interactions['look']['name'], "Look")
        self.assertEqual(interactions['look']['visible'], True)
        self.assertEqual(interactions['look']['fail_message'], None)
        self.assertEqual(interactions['use']['name'], "Use")
        self.assertEqual(interactions['use']['visible'], True)
        self.assertEqual(interactions['use']['fail_message'], None)
        self.assertEqual(interactions['pickup']['name'], "Pick Up")
        self.assertEqual(interactions['pickup']['visible'], True)
        self.assertEqual(interactions['pickup']['fail_message'], None)
        self.assertEqual(interactions['talk']['name'], "Talk To")
        self.assertEqual(interactions['talk']['visible'], True)
        self.assertEqual(interactions['talk']['fail_message'], None)
        self.assertEqual(interactions[5]['name'], "Sarc At")
        self.assertEqual(interactions[5]['visible'], True)
        self.assertEqual(interactions[5]['fail_message'], "Your sarcasm falls flat.")

    def test_get_interactions_after_hiding_custom_interaction(self):
        self.puzzle_player.puzzle['interactions'][5]['visible'] = False
        interactions = self.puzzle_player.get_current_player_interactions()
        self.assertEqual(len(interactions), 4)

    def test_get_interactions_after_hiding_interaction(self):
        self.puzzle_player.puzzle['interactions']['use']['visible'] = False
        interactions = self.puzzle_player.get_current_player_interactions()
        self.assertEqual(len(interactions), 4)

    def test_get_current_room(self):
        current_room = self.puzzle_player.get_current_room()
        self.assertEqual(current_room['name'], "Start")
        self.assertEqual(current_room['description'], "The start of the room")
        self.assertEqual(current_room['initial_message'], "You enter the start of the room")
        self.assertEqual(len(current_room['items']), 2)
        self.assertIn(1, current_room['items'])
        self.assertIn(4, current_room['items'])

    def test_get_current_room_after_trovelling(self):
        self.puzzle_player.travel_using_direction(1)
        current_room = self.puzzle_player.get_current_room()
        self.assertEqual(current_room['name'], "End")
        self.assertEqual(current_room['description'], "The end of the room")
        self.assertEqual(current_room['initial_message'], "You enter the end of the room")
        self.assertEqual(len(current_room['items']), 0)

    def test_do_enter_room_same_room(self):
        self.puzzle_player.do_enter_room()
        self.assertEqual(self.puzzle_player.puzzle['player']['room'], 1)
        self.assertEqual(self.puzzle_player.puzzle['rooms'][1]['num_times_entered'], 2)
        self.assertEqual(self.puzzle_player.messages[-1], self.puzzle_player.puzzle['rooms'][1]['description'])
        self.assertNotEqual(self.puzzle_player.messages[-2], self.puzzle_player.puzzle['rooms'][1]['initial_message'])

    def test_do_enter_room_different_room(self):
        self.puzzle_player.puzzle['player']['room'] = 2
        self.puzzle_player.do_enter_room()
        self.assertEqual(self.puzzle_player.puzzle['player']['room'], 2)
        self.assertEqual(self.puzzle_player.puzzle['rooms'][1]['num_times_entered'], 1)
        self.assertEqual(self.puzzle_player.puzzle['rooms'][2]['num_times_entered'], 1)
        self.assertIn(self.puzzle_player.puzzle['rooms'][2]['initial_message'], self.puzzle_player.messages)
        self.assertEqual(self.puzzle_player.messages[-1], self.puzzle_player.puzzle['rooms'][2]['description'])

    def test_travel_using_direction(self):
        self.puzzle_player.travel_using_direction(1)
        self.assertEqual(self.puzzle_player.puzzle['player']['room'], 2)
        self.assertIn(self.puzzle_player.puzzle['rooms'][2]['initial_message'], self.puzzle_player.messages)
        self.assertIn(self.puzzle_player.puzzle['rooms'][2]['description'], self.puzzle_player.messages)
        self.assertIn("You travel East", self.puzzle_player.messages)

    def test_travel_using_direction_custom_travel_message(self):
        self.puzzle_player.puzzle['rooms'][1]['directions'][1]['travel_message'] = "beep boop"
        self.puzzle_player.travel_using_direction(1)
        self.assertIn("beep boop", self.puzzle_player.messages)
        self.assertNotIn("You travel East", self.puzzle_player.messages)

    def test_travel_using_direction_hidden(self):
        self.puzzle_player.puzzle['rooms'][1]['directions'][1]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.travel_using_direction(1)

    def test_travel_using_direction_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.travel_using_direction(5)

    def test_travel_using_direction_not_in_room(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.travel_using_direction(2)

    def test_set_status(self):
        self.puzzle_player.action_player_set_location = MethodWrapper(self.puzzle_player.action_player_set_location)
        num_status = len(QuestPuzzleStatus.objects.filter(quest=self.quest))

        self.puzzle_player.set_status("player_set_location", None, 2)
        self.assertTrue(self.puzzle_player.action_player_set_location.was_called)
        statuses = QuestPuzzleStatus.objects.filter(quest=self.quest)
        self.assertEqual(len(statuses), num_status+1)
        self.assertEqual(statuses.reverse()[0].action_type, "player_set_location")

    def test_set_status_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.set_status("hello")

    def test_set_status_no_update(self):
        num_status = len(QuestPuzzleStatus.objects.filter(quest=self.quest))

        self.puzzle_player.set_status("player_set_location", None, 2, update_quest=False)
        statuses = QuestPuzzleStatus.objects.filter(quest=self.quest)
        self.assertEqual(len(statuses), num_status)

    def test_get_interaction_from_raw_action(self):
        interaction = self.puzzle_player.get_interaction_from_raw_action("look", "room:1")
        self.assertEqual(len(interaction), 3)
        self.assertEqual(interaction[0], "look")
        self.assertEqual(interaction[1], "room")
        self.assertEqual(interaction[2], 1)

    def test_get_interaction_from_raw_action_action_on_invalid_format(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.get_interaction_from_raw_action("look", ":room:1")

    def test_get_interaction_from_raw_action_action_on_pk_is_not_int(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.get_interaction_from_raw_action("look", "room:hello")

    def test_get_interaction_from_raw_action_action_on_invalid_location(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.get_interaction_from_raw_action("look", "poop:1")

    def test_show_default_builtin_interaction_message_item_name_replaces_properly(self):
        self.puzzle_player.show_default_builtin_interaction_message("look", "Cup")
        self.assertIn("Cup", self.puzzle_player.messages[-1])

    def test_show_default_builtin_interaction_message_use(self):
        self.puzzle_player.show_default_builtin_interaction_message("use", "Cup")
        self.assertEqual("That doesn't work.", self.puzzle_player.messages[-1])

    def test_show_default_builtin_interaction_message_pickup(self):
        self.puzzle_player.show_default_builtin_interaction_message("pickup", "Cup")
        self.assertEqual("You can't pick that up.", self.puzzle_player.messages[-1])

    def test_show_default_builtin_interaction_message_look(self):
        self.puzzle_player.show_default_builtin_interaction_message("look", "Cup")
        self.assertEqual("Nice Cup.", self.puzzle_player.messages[-1])

    def test_show_default_builtin_interaction_message_inventory(self):
        self.puzzle_player.show_default_builtin_interaction_message("inventory", "Cup")
        self.assertEqual("They can't be combined.", self.puzzle_player.messages[-1])


class PuzzlePlayerDoTurn(BaseTestCase):

    def test_do_turn_no_action(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn()

    def test_do_turn_invalid_action(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("poop")

    def test_finished_puzzles_throw_exception(self):
        self.puzzle_player.quest.is_finished = True
        self.puzzle_player.quest.save()
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("travel", 1)

    def test_do_turn_travel(self):
        self.puzzle_player.travel_using_direction = MethodWrapper(self.puzzle_player.travel_using_direction)
        self.puzzle_player.do_turn("travel", 1)
        self.assertTrue(self.puzzle_player.travel_using_direction.was_called)
        self.assertEqual(self.puzzle_player.puzzle['global']['turn_num'], 2)
        self.assertEqual(self.puzzle_player.puzzle['player']['room'], 2)

    def test_do_turn_travel_no_direction(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("travel")

    def test_do_turn_travel_exit_completes_puzzle(self):
        self.assertFalse(self.puzzle_player.quest.is_finished)
        self.puzzle_player.do_turn("travel", 1)
        self.puzzle_player.do_turn("travel", 3)
        self.assertTrue(self.puzzle_player.quest.is_finished)

    def test_do_turn_pickup(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 1)
        self.puzzle_player.do_turn("pickup", "room:1")
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 1)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 2)
        self.assertIn(1, self.puzzle_player.puzzle['player']['inventory'])
        self.assertNotIn(1, self.puzzle_player.puzzle['rooms'][1]['items'])

    def test_do_turn_pickup_no_action_on(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("pickup")

    def test_do_turn_pickup_invalid_action_on(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("pickup", 23)

    def test_do_turn_pickup_item_not_exist(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("pickup", "room:5")

    def test_do_turn_pickup_item_not_visible(self):
        self.puzzle_player.puzzle['items'][1]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("pickup", "room:1")

    def test_do_turn_pickup_item_not_pickupable(self):
        self.puzzle_player.puzzle['items'][1]['can_pickup'] = False
        self.puzzle_player.do_turn("pickup", "room:1")
        self.assertEqual("You can't pick that up.", self.puzzle_player.messages[-1])

    def test_do_turn_pickup_item_not_in_room(self):
        self.puzzle_player.puzzle['rooms'][1]['items'] = []
        self.puzzle_player.puzzle['rooms'][2]['items'] = [1]
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("pickup", "room:1")

    def test_do_turn_pickup_item_already_in_inventory(self):
        self.puzzle_player.do_turn("pickup", "inventory:2")
        self.assertEqual("You're already holding the Water Gun.", self.puzzle_player.messages[-1])

    def test_do_turn_pickup_item_is_nowhere(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("pickup", "room:3")

    def test_do_turn_pickup_try_to_pickup_room(self):
        self.puzzle_player.do_turn("pickup", "room:-1")

    def test_do_turn_pickup_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['pickup']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("pickup", "room:1")

    def test_do_turn_look_at_room(self):
        self.puzzle_player.do_turn("look", "room:-1")
        self.assertEqual("The start of the room", self.puzzle_player.messages[-1])

    def test_do_turn_look_at_room_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['look']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("look", "room:-1")

    def test_do_turn_look_at_item(self):
        self.puzzle_player.do_turn("look", "room:1")
        self.assertEqual("A crappy bucket.", self.puzzle_player.messages[-1])

    def test_do_turn_look_at_item_in_inventory(self):
        self.puzzle_player.do_turn("look", "room:2")
        self.assertEqual("A cute water gun.", self.puzzle_player.messages[-1])

    def test_do_turn_look_at_item_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("look", "room:55")

    def test_do_turn_look_at_item_not_visible(self):
        self.puzzle_player.puzzle['items'][1]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("look", "room:1")

    def test_do_turn_look_at_item_in_inventory_not_visible(self):
        self.puzzle_player.puzzle['items'][2]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("look", "inventory:2")

    def test_do_turn_look_at_item_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['look']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("look", "room:1")

    def test_do_turn_use_room(self):
        self.puzzle_player.do_turn("use", "room:-1")
        self.assertEqual("That doesn't work.", self.puzzle_player.messages[-1])

    def test_do_turn_use_room_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['use']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", "room:-1")

    def test_do_turn_use_item(self):
        self.puzzle_player.do_turn("use", "room:1")
        self.assertEqual("That doesn't work.", self.puzzle_player.messages[-1])

    def test_do_turn_use_item_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", "room:55")

    def test_do_turn_use_item_not_visible(self):
        self.puzzle_player.puzzle['items'][1]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", "room:1")

    def test_do_turn_use_item_in_inventory(self):
        self.puzzle_player.do_turn("use", "inventory:2")
        self.assertEqual("That doesn't work.", self.puzzle_player.messages[-1])

    def test_do_turn_use_item_in_inventory_not_visible(self):
        self.puzzle_player.puzzle['items'][2]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", "inventory:2")

    def test_do_turn_use_item_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['use']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", "room:1")

    def test_do_turn_use_item_in_inventory_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['use']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", "inventory:2")

    def test_do_turn_use_inventory_on_room(self):
        self.puzzle_player.do_turn("inventory:2", "room:-1")
        self.assertEqual("They can't be combined.", self.puzzle_player.messages[-1])

    def test_do_turn_use_inventory_on_inventory(self):
        self.puzzle_player.action_item_set_location(1, -1)
        self.puzzle_player.do_turn("inventory:2", "inventory:1")
        self.assertEqual("They can't be combined.", self.puzzle_player.messages[-1])

    def test_do_turn_use_inventory_on_inventory_same_item(self):
        self.puzzle_player.do_turn("inventory:2", "inventory:2")
        self.assertEqual("You can't use something on itself.", self.puzzle_player.messages[-1])

    def test_do_turn_use_inventory_on_item_on_bad_format(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:2", "room:1:")

    def test_do_turn_use_inventory_on_location_invalid(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:2", "boo:1")

    def test_do_turn_use_inventory_on_item_on_not_int(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:2", "room:bad")

    def test_do_turn_use_inventory_on_item_not_intt(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:terrible", "room:1")

    def test_do_turn_use_inventory_on_inventory_item_not_exist(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:45", "room:1")

    def test_do_turn_use_inventory_on_inventory_item_on_not_exist(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:2", "room:64")

    def test_do_turn_use_inventory_on_inventory_item_on_in_different_room(self):
        self.puzzle_player.action_item_set_location(1, 2)
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:2", "room:1")

    def test_do_turn_use_inventory_on_inventory_item_not_visible(self):
        self.puzzle_player.puzzle['items'][2]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:2", "room:1")

    def test_do_turn_use_inventory_on_inventory_item_on_not_visible(self):
        self.puzzle_player.puzzle['items'][1]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("inventory:2", "room:1")

    def test_do_turn_use_exit_item_completes_puzzle(self):
        self.puzzle_player.do_turn("use", "room:4")
        self.assertTrue(self.puzzle_player.quest.is_finished)

    def test_do_turn_use_exit_item_not_visible(self):
        self.puzzle_player.puzzle['items'][4]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", 4)
        self.assertFalse(self.puzzle_player.quest.is_finished)

    def test_do_turn_use_exit_item_in_inventory_completes_puzzle(self):
        self.puzzle_player.action_item_set_location(4, -1)
        self.puzzle_player.do_turn("use", "room:4")
        self.assertTrue(self.puzzle_player.quest.is_finished)

    def test_do_turn_use_exit_item_in_inventory_not_visible(self):
        self.puzzle_player.action_item_set_location(4, -1)
        self.puzzle_player.puzzle['items'][4]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("use", "inventory:4")
        self.assertFalse(self.puzzle_player.quest.is_finished)

    def test_do_turn_use_exit_item_in_inventory_on_item(self):
        self.puzzle_player.action_item_set_location(4, -1)
        self.puzzle_player.do_turn("inventory:4", "inventory:1")
        self.assertFalse(self.puzzle_player.quest.is_finished)

    def test_do_turn_talk_to_room(self):
        self.puzzle_player.do_turn("talk", "room:-1")
        self.assertEqual("It's not talking.", self.puzzle_player.messages[-1])

    def test_do_turn_talk_to_item(self):
        self.puzzle_player.do_turn("talk", "room:1")
        self.assertEqual("It's not talking.", self.puzzle_player.messages[-1])

    def test_do_turn_talk_to_item_in_inventory(self):
        self.puzzle_player.do_turn("talk", "room:2")
        self.assertEqual("It's not talking.", self.puzzle_player.messages[-1])

    def test_do_turn_talk_to_item_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("talk", "room:55")

    def test_do_turn_talk_to_item_not_visible(self):
        self.puzzle_player.puzzle['items'][1]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("talk", "room:1")

    def test_do_turn_talk_to_item_in_inventory_not_visible(self):
        self.puzzle_player.puzzle['items'][2]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("talk", "inventory:2")

    def test_do_turn_talk_to_room_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['talk']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("talk", "room:-1")

    def test_do_turn_talk_to_item_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['talk']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("talk", "room:1")

    def test_do_turn_talk_to_item_in_inventory_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions']['talk']['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn("talk", "inventory:2")

    def test_do_turn_custom_interaction(self):
        self.puzzle_player.do_turn(5, "room:1")
        self.assertEqual("Your sarcasm falls flat.", self.puzzle_player.messages[-1])

    def test_do_turn_custom_interaction_on_room(self):
        self.puzzle_player.do_turn(5, "room:-1")
        self.assertEqual("Your sarcasm falls flat.", self.puzzle_player.messages[-1])

    def test_do_turn_custom_interaction_on_inventory_item(self):
        self.puzzle_player.do_turn(5, "inventory:2")
        self.assertEqual("Your sarcasm falls flat.", self.puzzle_player.messages[-1])

    def test_do_turn_custom_interaction_interaction_not_visible(self):
        self.puzzle_player.puzzle['interactions'][5]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn(5, "room:1")

    def test_do_turn_custom_interaction_item_not_visible(self):
        self.puzzle_player.puzzle['items'][1]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn(5, "room:1")

    def test_do_turn_custom_interaction_item_in_inventory_not_visible(self):
        self.puzzle_player.puzzle['items'][2]['visible'] = False
        with self.assertRaises(PuzzlePlayerValidationException):
            self.puzzle_player.do_turn(5, "inventory:2")


class PuzzlePlayerActions(BaseTestCase):

    def test_turn_num(self):
        self.puzzle_player.action_turn_num(None, 5)
        self.assertEqual(self.puzzle_player.puzzle['global']['turn_num'], 5)

    def test_turn_num_nan(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_turn_num(None, "test")

    def test_room_num_times_entered(self):
        self.puzzle_player.action_room_num_times_entered(1, 10)
        self.assertEqual(self.puzzle_player.puzzle['rooms'][1]['num_times_entered'], 10)

    def test_room_num_times_entered_invalid_room(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_room_num_times_entered(20, 10)

    def test_room_num_times_entered_nan(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_room_num_times_entered(2, "test")

    def test_player_set_location(self):
        self.puzzle_player.action_player_set_location(None, 2)
        self.assertEqual(self.puzzle_player.puzzle['player']['room'], 2)

    def test_player_set_location_invalid_room(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_player_set_location(None, 20)

    def test_item_set_location_to_inventory(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 1)
        self.puzzle_player.action_item_set_location(1, -1)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 1)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 2)
        self.assertIn(1, self.puzzle_player.puzzle['player']['inventory'])
        self.assertNotIn(1, self.puzzle_player.puzzle['rooms'][1]['items'])

    def test_item_set_location_to_room(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['items']), 0)
        self.puzzle_player.action_item_set_location(1, 2)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 1)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['items']), 1)
        self.assertNotIn(1, self.puzzle_player.puzzle['player']['inventory'])
        self.assertIn(1, self.puzzle_player.puzzle['rooms'][2]['items'])
        self.assertNotIn(1, self.puzzle_player.puzzle['rooms'][1]['items'])

    def test_item_set_location_to_nowhere(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.puzzle_player.action_item_set_location(1, 0)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 1)
        self.assertNotIn(1, self.puzzle_player.puzzle['player']['inventory'])
        self.assertNotIn(1, self.puzzle_player.puzzle['rooms'][1]['items'])
        self.assertNotIn(1, self.puzzle_player.puzzle['rooms'][2]['items'])

    def test_item_set_location_to_same_room(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['items']), 0)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 1)
        self.puzzle_player.action_item_set_location(1, 1)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['items']), 0)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 1)
        self.assertNotIn(1, self.puzzle_player.puzzle['player']['inventory'])
        self.assertIn(1, self.puzzle_player.puzzle['rooms'][1]['items'])
        self.assertNotIn(1, self.puzzle_player.puzzle['rooms'][2]['items'])

    def test_item_set_location_to_inventory_if_in_inventory(self):
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['items']), 0)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 1)
        self.puzzle_player.action_item_set_location(2, -1)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][1]['items']), 2)
        self.assertEqual(len(self.puzzle_player.puzzle['rooms'][2]['items']), 0)
        self.assertEqual(len(self.puzzle_player.puzzle['player']['inventory']), 1)
        self.assertIn(2, self.puzzle_player.puzzle['player']['inventory'])
        self.assertNotIn(2, self.puzzle_player.puzzle['rooms'][1]['items'])
        self.assertNotIn(2, self.puzzle_player.puzzle['rooms'][2]['items'])

    def test_item_set_location_item_not_number(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_item_set_location("boop", 1)

    def test_item_set_location_location_not_number(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_item_set_location(1, "boop")

    def test_item_set_location_item_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_item_set_location(55, 1)

    def test_item_set_location_room_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_item_set_location(1, 55)

    def test_interaction_visibility(self):
        self.assertEqual(self.puzzle_player.puzzle['interactions']['use']['visible'], True)
        self.assertEqual(self.puzzle_player.puzzle['interactions']['look']['visible'], True)
        self.puzzle_player.action_interaction_set_visibility("use", False)
        self.assertEqual(self.puzzle_player.puzzle['interactions']['use']['visible'], False)
        self.assertEqual(self.puzzle_player.puzzle['interactions']['look']['visible'], True)
        self.puzzle_player.action_interaction_set_visibility("use", True)
        self.assertEqual(self.puzzle_player.puzzle['interactions']['use']['visible'], True)

    def test_interaction_visibility_custom(self):
        self.assertEqual(self.puzzle_player.puzzle['interactions'][5]['visible'], True)
        self.assertEqual(self.puzzle_player.puzzle['interactions']['look']['visible'], True)
        self.puzzle_player.action_interaction_set_visibility(5, False)
        self.assertEqual(self.puzzle_player.puzzle['interactions'][5]['visible'], False)
        self.assertEqual(self.puzzle_player.puzzle['interactions']['look']['visible'], True)

    def test_interaction_visibility_is_not_number_or_string(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_interaction_set_visibility(["test"], False)

    def test_interaction_visibility_number_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_interaction_set_visibility(10, False)

    def test_interaction_visibility_string_does_not_exist(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_interaction_set_visibility("poop", False)

    def test_interaction_visibility_value_is_not_bool(self):
        with self.assertRaises(PuzzlePlayerException):
            self.puzzle_player.action_interaction_set_visibility("use", "nope")
