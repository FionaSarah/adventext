from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse

from .quest import PuzzlePlayer, PuzzlePlayerValidationException
from .models import Quest
from ..puzzle.models import Puzzle
from ..puzzle.views import allowed_to_build_decorator


class PlayView(TemplateView):
    template_name = "play/test.html"
    redirects = {
        'serious_error' : reverse_lazy("index")
        }

    # PuzzlePlayer instance for this view
    puzzle_player = None

    # Puzzle model object to be played
    puzzle = None

    # Quest model object to be played
    quest = None

    # Set to True if it's the first time a puzzle is run
    starting_puzzle = False

    def get_puzzle_object(self):
        """Override to return the puzzle object from the DB for
        the current quest."""
        raise NotImplementedError

    def get_quest_object(self):
        """Override to return the quest object from the DB that
        will be played."""
        raise NotImplementedError

    def init_puzzle_player(self):
        self.puzzle_player = PuzzlePlayer(self.starting_puzzle, self.quest, self.puzzle)

    def init_view(self):
        try:
            self.quest = self.get_quest_object()
            self.puzzle = self.get_puzzle_object()
        except Puzzle.DoesNotExist:
            messages.success(self.request, "Requested puzzle does not exist.")
            return HttpResponseRedirect(self.redirects['serious_error'])
        self.init_puzzle_player()

    def dispatch(self, *args, **kwargs):
        r = self.init_view()
        if not r is None:
            return r
        return super(PlayView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        # catch dead puzzles and just render out without doing actions
        if self.quest.is_finished or self.quest.is_broken:
            # Catch the continue button
            if 'continue' in request.POST and request.POST['continue'] == "1":
                return self.continue_to_next_puzzle(request, *args, **kwargs)
            return self.render_to_response(self.get_context_data(**kwargs))

        action = None
        action_on = None
        # Determine if we are travelling
        if 'do_travel' in request.POST and \
               'travel' in request.POST and \
               request.POST['do_travel'] == "1" and \
               not request.POST['travel'] == "-1" :
            try:
                action_on = int(request.POST['travel'])
                action = "travel"
            except ValueError as e:
                messages.error(self.request, "Error travelling to passed room.")
                # TODO: Break quests
        # Or if we're doing an action
        elif 'do_action' in request.POST and \
                 'action' in request.POST and \
                 'item' in request.POST and \
                 request.POST['do_action'] == "1":
            action = request.POST['action']
            action_on = request.POST['item']
        # Only do the turn if there's an action to attend to
        if not action is None:
            try:
                self.puzzle_player.do_turn(action, action_on)
                return HttpResponseRedirect(request.get_full_path())
            except PuzzlePlayerValidationException as e:
                messages.warning(self.request, e)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def continue_to_next_puzzle(self, request, *args, **kwargs):
        pass

    def get_context_data(self, **kwargs):
        context = super(PlayView, self).get_context_data(**kwargs)
        context['puzzle_player'] = self.puzzle_player
        context['quest'] = self.quest
        context['message_history'] = self.puzzle_player.history
        context['current_messages'] = self.puzzle_player.messages
        context['current_directions'] = self.puzzle_player.get_current_player_directions()
        context['current_room_items'] = self.puzzle_player.get_items_for_current_room()
        context['inventory'] = self.puzzle_player.get_items_in_inventory()
        context['interactions'] = []
        interactions = self.puzzle_player.get_current_player_interactions()
        for pk in list(interactions.keys()):
            single_interaction = interactions[pk].copy()
            single_interaction['pk'] = pk
            context['interactions'].append(single_interaction)
        return context


class BuildTestView(PlayView):
    template_name = "puzzle/build_test.html"
    redirects = {
        'serious_error' : reverse_lazy("puzzle:index")
        }

    def get_puzzle_object(self):
        return self.puzzle

    def get_quest_object(self):
        self.puzzle = Puzzle.objects.get(
            pk=self.kwargs['puzzle_pk'], author=self.request.user, approved=0
            )
        try:
            q = Quest.objects.get(
                quest_type="build",
                user=self.request.user,
                current_puzzle=self.puzzle
                )
        except Quest.DoesNotExist:
            q = Quest(
                quest_type="build",
                user=self.request.user,
                current_puzzle=self.puzzle
                )
            q.save()
            self.starting_puzzle = True
        return q

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(BuildTestView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BuildTestView, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def continue_to_next_puzzle(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse("puzzle:test_restart", kwargs={'puzzle_pk':self.puzzle.pk}))


class BuildTestRestartView(BuildTestView):
    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        try:
            quest = self.get_quest_object()
        except Quest.DoesNotExist:
            return HttpResponseRedirect(reverse("puzzle:test"))
        puzzle = self.get_puzzle_object()
        quest.delete()
        messages.success(self.request, "Quest restarted.")
        return HttpResponseRedirect(reverse("puzzle:test", kwargs={'puzzle_pk':puzzle.pk}))
