from django import forms
from django.contrib import admin

from .models import Puzzle
from .models import Interaction
from .models import Item
from .models import Room
from .models import RoomDirection
from .models import Variable
from .models import Event
from .models import EventTrigger
from .models import EventAction
from .models import ActionGroup
from .models import ActionGroupAction

admin.site.register(Interaction)
admin.site.register(Item)
admin.site.register(RoomDirection)
admin.site.register(Variable)
admin.site.register(Event)
admin.site.register(EventTrigger)
admin.site.register(EventAction)
admin.site.register(ActionGroup)
admin.site.register(ActionGroupAction)

class PuzzleAdminForm(forms.ModelForm):
    class Meta:
        model = Puzzle
        exclude = []

    def __init__(self, *args, **kwargs):
        super(PuzzleAdminForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['start_room'].queryset = Room.objects.filter(puzzle = self.instance)

@admin.register(Puzzle)
class PuzzleAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'difficulty')
    form = PuzzleAdminForm

@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ('name', 'puzzle')
