from django import forms
from django.template import loader
from django.template import Context
from django.template.loader import get_template

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, HTML
from crispy_forms.bootstrap import FormActions, StrictButton

from .models import Puzzle, Room, RoomDirection, Item, Interaction, \
     Event, Variable, EventTrigger, EventAction
from adventext.play.quest import event_triggers, event_trigger_categories

class BuildPuzzleCreateForm(forms.Form):
    name = forms.CharField(
        label = "Puzzle Name",
        required = True
        )

    def __init__(self, *args, **kwargs):
        super(BuildPuzzleCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            'name',
            FormActions(Submit('submit', 'Start Puzzle'))
            )


class BuildPuzzleInfoForm(forms.ModelForm):
    class Meta:
        model = Puzzle
        fields = ["name", "difficulty", "start_room", "entrance_text", "solution"]
        labels = {
            "name" : "Puzzle Name",
            "solution" : "Puzzle solution",
        }
        help_texts = {
            "entrance_text" : "Text that is displayed when starting the puzzle",
            "solution" : "Used to help the approval process."
            }

    def __init__(self, *args, **kwargs):
        super(BuildPuzzleInfoForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['start_room'].queryset = Room.objects.filter(puzzle = self.instance)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            'name',
            "difficulty",
            "start_room",
            "entrance_text",
            "solution",
            FormActions(Submit('submit', 'Update Puzzle'))
            )


class BuildRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ["name", "description", "initial_message"]
        labels = {
            "initial_message" : "First-time entrance description"
        }

    def __init__(self, *args, **kwargs):
        super(BuildRoomForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            'name',
            "description",
            "initial_message",
            FormActions(
                Submit('submit', "Update Room" if self.instance.pk else 'Add New Room')
                )
            )


class BuildDirectionForm(forms.ModelForm):
    manual_destination = forms.ChoiceField(
        label = "Room this direction links to",
        required = True
        )

    class Meta:
        model = RoomDirection
        fields = ["name", "manual_destination", "initially_visible", "travel_message"]
        labels = {
            "travel_message":"Message when traveling in direction",
            "initially_visible":"Direction visible by default?"
        }
        help_texts = {
            "name":"e.g. North, South, In, Up etc.",
            }

    def __init__(self, *args, **kwargs):
        puzzle = kwargs.pop("puzzle")
        room = kwargs.pop("room")
        super(BuildDirectionForm, self).__init__(*args, **kwargs)
        if self.instance:
            choices = [("exit", "[ Exit Puzzle ]")]
            for r in Room.objects.filter(puzzle = puzzle):
                if room == r:
                    continue
                choices.append((r.pk, r.name))
            self.fields['manual_destination'].choices = choices
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            "name",
            "manual_destination",
            "travel_message",
            "initially_visible",
            FormActions(
                Submit('submit', "Update Direction" if self.instance.pk else 'Add New Direction')
                )
            )


class BuildItemForm(forms.ModelForm):
    manual_location = forms.ChoiceField(
        label = "Initial item location",
        required = True
        )

    class Meta:
        model = Item
        fields = ["name", "description", "is_exit", "can_pickup", "visible", "manual_location"]
        labels = {
            "is_exit":"Item is a puzzle exit?",
            "can_pickup":"Player can <strong>pick-up</strong> this item?",
            "visible":"Item is initially visible",
        }
        help_texts = {
            "description":"The text that appears when you <strong>look</strong> at an item.",
            }

    def __init__(self, *args, **kwargs):
        puzzle = kwargs.pop("puzzle")
        super(BuildItemForm, self).__init__(*args, **kwargs)
        if self.instance:
            choices = [(-1, "[ Player Inventory ]"), (0, "[ Nowhere ]")]
            for r in Room.objects.filter(puzzle = puzzle):
                choices.append((r.pk, r.name))
            self.fields['manual_location'].choices = choices
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            "name",
            "description",
            "manual_location",
            "is_exit",
            "can_pickup",
            "visible",
            FormActions(
                Submit('submit', "Update Item" if self.instance.pk else 'Add New Item')
                )
            )

    def clean_manual_location(self):
        val = self.cleaned_data['manual_location']
        if val in ("-1", "0"):
            return val
        try:
            Room.objects.get(pk=int(val))
        except (ValueError, Room.DoesNotExist):
            raise forms.ValidationError("Invalid room passed.")
        return val


class BuiltInCantEditWidget(forms.Widget):
    def render(self, name, value, attrs):
        return "<strong>Cannot edit a default interaction's failure message.</strong>"


class BuildInteractionForm(forms.ModelForm):
    class Meta:
        model = Interaction
        fields = ["name", "fail_message", "initially_visible"]
        labels = {
            "name":"Name",
            "fail_message":"Message upon interaction failure",
            "initially_visible":"Interaction is initially visible",
        }
        help_texts = {
            "fail_message":"For instance, if you can't pick up an item when attempting to.<br />Use {} to insert the name of the object you're interacting with.",
            }

    def __init__(self, *args, **kwargs):
        puzzle = kwargs.pop("puzzle")
        super(BuildInteractionForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.built_in:
            self.fields['fail_message'].widget = BuiltInCantEditWidget()
            self.fields['fail_message'].help_text = None
            self.fields['fail_message'].required = False
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            "name",
            "initially_visible",
            "fail_message",
            FormActions(
                Submit('submit', "Update Interaction" if self.instance.pk else 'Add New Item')
                )
            )


class BuildEventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ["name", "repeats"]
        labels = {
            "repeats" : "Times to repeat"
        }
        help_texts = {
            "name" : "This name is just for you and never shown to the player.",
            "repeats" : "If set to 0, the event will repeat forever."
        }

    def __init__(self, *args, **kwargs):
        puzzle = kwargs.pop("puzzle")
        super(BuildEventForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            'name',
            "repeats",
            FormActions(
                Submit('submit', "Update Event" if self.instance.pk else 'Add New Event')
                )
            )


class TriggerActionFormBase(forms.ModelForm):
    def get_html_for_params(self, params):
        html = ""

        class TempForm(forms.Form):
            pass

        num = 0
        for param_type, param_name in params.items():
            setattr(
                TempForm, 'param_%s' % num,
                forms.CharField(label = param_name, required = True)
                )
            num+=1

        temp_form = TempForm()

        for param_num in range(num):
            template = get_template('bootstrap4/field.html')
            c = Context({'field':getattr(temp_form, 'param_%s' % param_num),
                         'form_show_errors': True})
            html += template.render(c)

        return html


class BuildEventTriggerForm(TriggerActionFormBase):
    trigger_type = forms.ChoiceField(
        label = "Trigger",
        required = True
        )

    class Meta:
        model = EventTrigger
        fields = ["trigger_type"]
        labels = {
            "trigger_type":"Trigger"
        }

    def __init__(self, *args, **kwargs):
        puzzle = kwargs.pop("puzzle")
        event = kwargs.pop("event")
        super(BuildEventTriggerForm, self).__init__(*args, **kwargs)

        # Set up trigger type dropdown
        self.fields['trigger_type'].choices = [('', '-')]
        for category_key, category_name in event_trigger_categories.items():
            cat = [category_name, []]
            for trigger_key, trigger in event_triggers.items():
                if not trigger['category'] == category_key:
                    continue
                cat[1].append([trigger_key, trigger['name']])
            self.fields['trigger_type'].choices.append(cat)

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'
        self.helper.layout = Layout(
            "trigger_type",
            HTML(loader.get_template("puzzle/async_form_field.html").render()),
            HTML('<div class="description"></div>'),
            HTML('<div class="params"></div>'),
            HTML('<div class="secondary_params"></div>'),
            )
        if self.instance.pk:
            self.helper.layout.append(
                FormActions(Submit('submit', "Update Trigger"))
                )
