# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ActionGroupAction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('action_type', models.CharField(max_length=255)),
                ('param1', models.TextField()),
                ('param2', models.TextField()),
                ('param3', models.TextField()),
                ('param4', models.TextField()),
                ('param5', models.TextField()),
                ('group', models.ForeignKey(to='puzzle.ActionGroup')),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('priority', models.IntegerField(default=0)),
                ('repeats', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='EventAction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('action_type', models.CharField(max_length=255)),
                ('param1', models.TextField()),
                ('param2', models.TextField()),
                ('param3', models.TextField()),
                ('param4', models.TextField()),
                ('param5', models.TextField()),
                ('event', models.ForeignKey(to='puzzle.Event')),
            ],
        ),
        migrations.CreateModel(
            name='EventTrigger',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('trigger_type', models.CharField(max_length=255)),
                ('param1', models.TextField()),
                ('param2', models.TextField()),
                ('param3', models.TextField()),
                ('param4', models.TextField()),
                ('param5', models.TextField()),
                ('event', models.ForeignKey(to='puzzle.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Interaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('built_in_short_name', models.CharField(max_length=128, choices=[('look', 'Look'), ('use', 'Use'), ('pickup', 'Pick Up'), ('talk', 'Talk To')])),
                ('built_in', models.BooleanField(default=False)),
                ('initially_visible', models.BooleanField(default=True)),
                ('fail_message', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('descritpion', models.TextField()),
                ('is_exit', models.BooleanField(default=False)),
                ('can_pickup', models.BooleanField(default=False)),
                ('visible', models.BooleanField(default=True)),
                ('location', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Puzzle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('difficulty', models.CharField(max_length=16, choices=[('easy', 'Easy'), ('medium', 'Medium'), ('hard', 'Hard')])),
                ('entrance_text', models.TextField()),
                ('solution', models.TextField()),
                ('approved', models.BooleanField(default=False)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('descritpion', models.TextField()),
                ('initial_message', models.TextField()),
                ('puzzle', models.ForeignKey(to='puzzle.Puzzle')),
            ],
        ),
        migrations.CreateModel(
            name='RoomDirection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('is_exit', models.BooleanField(default=False)),
                ('initially_visible', models.BooleanField(default=True)),
                ('travel_message', models.TextField()),
                ('destination', models.ForeignKey(to='puzzle.Room', related_name='direction_destination')),
                ('puzzle', models.ForeignKey(to='puzzle.Puzzle')),
                ('room', models.ForeignKey(to='puzzle.Room', related_name='direction_room')),
            ],
        ),
        migrations.CreateModel(
            name='Variable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('default_value', models.TextField()),
                ('value_type', models.CharField(max_length=16, choices=[('bool', 'Yes/No flag'), ('int', 'Whole number'), ('float', 'Decimal number'), ('string', 'Text')])),
                ('puzzle', models.ForeignKey(to='puzzle.Puzzle')),
            ],
        ),
        migrations.AddField(
            model_name='puzzle',
            name='start_room',
            field=models.ForeignKey(to='puzzle.Room', related_name='start_room_for_puzzle'),
        ),
        migrations.AddField(
            model_name='item',
            name='puzzle',
            field=models.ForeignKey(to='puzzle.Puzzle'),
        ),
        migrations.AddField(
            model_name='interaction',
            name='puzzle',
            field=models.ForeignKey(to='puzzle.Puzzle'),
        ),
        migrations.AddField(
            model_name='eventtrigger',
            name='puzzle',
            field=models.ForeignKey(to='puzzle.Puzzle'),
        ),
        migrations.AddField(
            model_name='eventaction',
            name='puzzle',
            field=models.ForeignKey(to='puzzle.Puzzle'),
        ),
        migrations.AddField(
            model_name='event',
            name='puzzle',
            field=models.ForeignKey(to='puzzle.Puzzle'),
        ),
        migrations.AddField(
            model_name='actiongroupaction',
            name='puzzle',
            field=models.ForeignKey(to='puzzle.Puzzle'),
        ),
        migrations.AddField(
            model_name='actiongroup',
            name='puzzle',
            field=models.ForeignKey(to='puzzle.Puzzle'),
        ),
    ]
