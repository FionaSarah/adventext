# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('puzzle', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='puzzle',
            name='start_room',
            field=models.ForeignKey(to='puzzle.Room', related_name='start_room_for_puzzle', null=True),
        ),
    ]
