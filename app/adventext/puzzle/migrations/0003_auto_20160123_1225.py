# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('puzzle', '0002_auto_20151025_1554'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='descritpion',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='room',
            old_name='descritpion',
            new_name='description',
        ),
    ]
