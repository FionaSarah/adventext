# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('puzzle', '0003_auto_20160123_1225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='puzzle',
            name='entrance_text',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='puzzle',
            name='solution',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='initial_message',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='roomdirection',
            name='destination',
            field=models.ForeignKey(related_name='direction_destination', null=True, to='puzzle.Room'),
        ),
        migrations.AlterField(
            model_name='roomdirection',
            name='travel_message',
            field=models.TextField(blank=True),
        ),
    ]
