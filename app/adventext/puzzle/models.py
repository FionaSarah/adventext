from django.db import models
from adventext.users.models import User


class Puzzle(models.Model):
    """
    Basic common puzzle data.
    """
    def limit_start_room_choices(self):
        return {'puzzle' : self.pk}

    difficulty_choices = (
          ('easy', 'Easy'),
          ('medium', 'Medium'),
          ('hard', 'Hard'),
        )
    name = models.CharField(max_length = 255)
    author = models.ForeignKey(User)
    difficulty = models.CharField(max_length = 16, choices = difficulty_choices)
    start_room = models.ForeignKey('Room', related_name = 'start_room_for_puzzle', null = True)
    entrance_text = models.TextField(blank = True)
    solution = models.TextField(blank = True)
    approved = models.BooleanField(default = False)

    def __str__(self):
        return self.name + " by " + self.author.username

    def save(self, force_insert=False, force_update=False):
        if self.start_room is not None and self.start_room.puzzle != self:
            raise Exception("You must select a room that's part of this puzzle as the start room.")
        super(Puzzle, self).save(force_insert, force_update)


class Interaction(models.Model):
    """
    Interactions are the actions that the player users to interact with
    the world or objects
    """
    built_in_choices = (
          ('look', 'Look'),
          ('use', 'Use'),
          ('pickup', 'Pick Up'),
          ('talk', 'Talk To'),
        )
    puzzle = models.ForeignKey(Puzzle)
    name = models.CharField(max_length = 255)
    built_in_short_name = models.CharField(max_length = 128, choices = built_in_choices)
    built_in = models.BooleanField(default = False)
    initially_visible = models.BooleanField(default = True)
    fail_message = models.TextField()

    def __str__(self):
        return self.name


class Item(models.Model):
    """
    Items that are in the player inventory or rooms that players
    can pick up and interact with
    """
    puzzle = models.ForeignKey(Puzzle)
    name = models.CharField(max_length = 255)
    description = models.TextField()
    is_exit = models.BooleanField(default = False)
    can_pickup = models.BooleanField(default = False)
    visible = models.BooleanField(default = True)
    location = models.IntegerField(default = 0)

    def location_name(self):
        if self.location == -1:
            return "[ Player Inventory ]"
        elif self.location == 0:
            return "[ Nowhere ]"
        else:
            try:
                r = Room.objects.get(pk=self.location)
                return r.name
            except Room.DoesNotExist:
                return "?"

    def __str__(self):
        return self.name


class Room(models.Model):
    """
    Rooms are what puzzles are made up of, players can move
    between them.
    """
    puzzle = models.ForeignKey(Puzzle)
    name = models.CharField(max_length = 255)
    description = models.TextField(blank = True)
    initial_message = models.TextField(blank = True)

    def __str__(self):
        return self.name


class RoomDirection(models.Model):
    """
    Directions are the primary way of travelling between
    rooms.
    """
    puzzle = models.ForeignKey(Puzzle)
    room = models.ForeignKey(Room, related_name = 'direction_room')
    destination = models.ForeignKey(Room, null = True, related_name = 'direction_destination')
    name = models.CharField(max_length = 255)
    is_exit = models.BooleanField(default = False)
    initially_visible = models.BooleanField(default = True)
    travel_message = models.TextField(blank = True)

    def __str__(self):
        return self.name


class Variable(models.Model):
    """
    Variables are used to store data arbitrary during puzzles.
    """
    value_type_choices = (
          ('bool', 'Yes/No flag'),
          ('int', 'Whole number'),
          ('float', 'Decimal number'),
          ('string', 'Text')
        )

    puzzle = models.ForeignKey(Puzzle)
    name = models.CharField(max_length = 255)
    default_value = models.TextField()
    value_type = models.CharField(max_length = 16, choices = value_type_choices)

    def __str__(self):
        return self.name + " (" + self.value_type + ")"


class Event(models.Model):
    """
    Events are containers of triggers and actions. Triggers
    of each event are checked after every interaction. If the
    trigger conditions are met then the actions on the event are run.
    They are checked in priority order.
    Events can be triggered as many times as the repeats value. A repeat
    value of 0 will repeat ad infinitum.
    """
    puzzle = models.ForeignKey(Puzzle)
    name = models.CharField(max_length = 255)
    priority = models.IntegerField(default = 0)
    repeats = models.IntegerField(default = 1)

    def __str__(self):
        return self.name


class EventTrigger(models.Model):
    """
    These are event triggers. When their conditions
    are met the relevant actions are run.
    """
    puzzle = models.ForeignKey(Puzzle)
    event = models.ForeignKey(Event)
    trigger_type = models.CharField(max_length = 255)
    param1 = models.TextField()
    param2 = models.TextField()
    param3 = models.TextField()
    param4 = models.TextField()
    param5 = models.TextField()



class EventAction(models.Model):
    """
    After events trigger conditions have been met
    then the actions will be run.
    """
    puzzle = models.ForeignKey(Puzzle)
    event = models.ForeignKey(Event)
    action_type = models.CharField(max_length = 255)
    param1 = models.TextField()
    param2 = models.TextField()
    param3 = models.TextField()
    param4 = models.TextField()
    param5 = models.TextField()


class ActionGroup(models.Model):
    """
    Action groups represent reusable chunks of actions
    that can be called by multiple triggers.
    """
    puzzle = models.ForeignKey(Puzzle)
    name = models.CharField(max_length = 255)

    def __str__(self):
        return self.name


class ActionGroupAction(models.Model):
    """
    Invidual actions in a group
    """
    puzzle = models.ForeignKey(Puzzle)
    group = models.ForeignKey(ActionGroup)
    action_type = models.CharField(max_length = 255)
    param1 = models.TextField()
    param2 = models.TextField()
    param3 = models.TextField()
    param4 = models.TextField()
    param5 = models.TextField()
