from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse
from django.contrib.staticfiles.templatetags.staticfiles import static

import django_tables2 as tables
from django_tables2.utils import A
from django.utils.html import escape

from .models import Puzzle, Room, RoomDirection, Item, Interaction, Event, \
     EventTrigger, EventAction, ActionGroup,  ActionGroupAction, Variable


def action_buttons(buttons):
    button_html = []
    for name, url in buttons:
        button_html.append(
            "<a href=\"{}\" class=\"btn btn-secondary btn-sm\">{}</a>".format(url, name)
            )
    return mark_safe(" ".join(button_html))


class PuzzleTable(tables.Table):
    name = tables.LinkColumn(
        'puzzle:index',
        text=lambda record: record.name,
        args=[A('pk')],
        verbose_name="Name")
    difficulty = tables.Column(
        verbose_name="Difficulty")
    actions = tables.Column(empty_values=(), orderable=False,
        verbose_name=mark_safe("&nbsp;"))

    def render_actions(self, record):
        return action_buttons([
            ("Build", reverse("puzzle:index", args=[record.pk])),
            ("Submit For Approval", reverse("puzzle:index", args=[record.pk])),
            ("Delete", reverse("puzzle:delete", args=[record.pk])),
            ])

    class Meta:
        template = "table.html"
        model = Puzzle
        fields = ("name", "difficulty", "actions")
        empty_text = "You haven't started building any puzzles yet."
        attrs = {'class': 'table table-bordered table-striped'}


class RoomTable(tables.Table):
    name = tables.LinkColumn(
        'puzzle:room_update',
        text=lambda record: record.name,
        kwargs={'puzzle_pk':A('puzzle.pk'), 'room_pk':A('pk')},
        verbose_name="Name")
    description = tables.Column(verbose_name="Description")
    actions = tables.Column(
        empty_values=(),
        orderable=False,
        attrs={"th": {"style": "width:250px;"}},
        verbose_name=mark_safe("&nbsp;")
        )

    def render_description(self, value):
        if len(value) > 80:
            return value[:80] + ". . ."
        return value

    def render_actions(self, record):
        room_kwargs = {'puzzle_pk':record.puzzle.pk, 'room_pk':record.pk}
        actions = [
            ("Edit", reverse("puzzle:room_update", kwargs=room_kwargs)),
            ("Edit Directions", reverse("puzzle:direction_list", kwargs=room_kwargs)),
            ]
        if len(self.rows) > 1:
            actions.append(("Delete", reverse("puzzle:room_delete", kwargs=room_kwargs)))
        return action_buttons(actions)

    class Meta:
        template = "table.html"
        model = Room
        fields = ("name", "description", "actions")
        empty_text = "No rooms in puzzle. That's unusual."
        attrs = {'class': 'table table-bordered table-striped'}


class RoomDirectionTable(tables.Table):
    name = tables.LinkColumn(
        'puzzle:direction_update',
        text=lambda record: record.name,
        kwargs={'puzzle_pk':A('puzzle.pk'), 'direction_pk':A('pk'), 'room_pk':A('room.pk')},
        verbose_name="Name")
    destination = tables.Column(verbose_name="Destination", empty_values=())
    initially_visible = tables.BooleanColumn(verbose_name="Initially Visible?")
    actions = tables.Column(
        empty_values=(),
        orderable=False,
        attrs={"th": {"style": "width:250px;"}},
        verbose_name=mark_safe("&nbsp;")
        )

    def render_destination(self, record):
        return "[ Exit Puzzle ]" if record.is_exit == True else record.destination.name

    def render_actions(self, record):
        dir_kwargs = {
            'puzzle_pk':record.puzzle.pk,
            'room_pk':record.room.pk,
            'direction_pk':record.pk
            }
        return action_buttons([
            ("Edit", reverse("puzzle:direction_update", kwargs=dir_kwargs)),
            ("Delete", reverse("puzzle:direction_delete", kwargs=dir_kwargs)),
            ])

    class Meta:
        template = "table.html"
        model = RoomDirection
        fields = ("name", "destination", "initially_visible", "actions")
        empty_text = "No directions created in this  room."
        attrs = {'class': 'table table-bordered table-striped'}


class ItemTable(tables.Table):
    name = tables.LinkColumn(
        'puzzle:item_update',
        text=lambda record: record.name,
        kwargs={'puzzle_pk':A('puzzle.pk'), 'item_pk':A('pk')},
        verbose_name="Name")
    location = tables.Column(verbose_name="Location", empty_values=())
    can_pickup = tables.BooleanColumn(verbose_name="Can Pickup?")
    actions = tables.Column(
        empty_values=(),
        orderable=False,
        attrs={"th": {"style": "width:250px;"}},
        verbose_name=mark_safe("&nbsp;")
        )

    def render_location(self, record):
        return record.location_name()

    def render_actions(self, record):
        item_kwargs = {'puzzle_pk':record.puzzle.pk, 'item_pk':record.pk}
        return action_buttons([
            ("Edit", reverse("puzzle:item_update", kwargs=item_kwargs)),
            ("Delete", reverse("puzzle:item_delete", kwargs=item_kwargs))
            ])

    class Meta:
        template = "table.html"
        model = Item
        fields = ("name", "location", "can_pickup", "actions")
        empty_text = "No items were found in puzzle."
        attrs = {'class': 'table table-bordered table-striped'}


class InteractionTable(tables.Table):
    name = tables.LinkColumn(
        'puzzle:interaction_update',
        text=lambda record: record.name,
        kwargs={'puzzle_pk':A('puzzle.pk'), 'interaction_pk':A('pk')},
        verbose_name="Name")
    initially_visible = tables.BooleanColumn(verbose_name="Initially Visible")
    actions = tables.Column(
        empty_values=(),
        orderable=False,
        attrs={"th": {"style": "width:250px;"}},
        verbose_name=mark_safe("&nbsp;")
        )

    def render_actions(self, record):
        interaction_kwargs = {'puzzle_pk':record.puzzle.pk, 'interaction_pk':record.pk}
        actions = [("Edit", reverse("puzzle:interaction_update", kwargs=interaction_kwargs))]
        if not record.built_in:
            actions.append(("Delete", reverse("puzzle:interaction_delete", kwargs=interaction_kwargs)))
        return action_buttons(actions)

    class Meta:
        template = "table.html"
        model = Interaction
        fields = ("name", "initially_visible", "actions")
        empty_text = "No interactions were found in puzzle. Urm that's super unusual. Maybe you should tell someone?"
        attrs = {'class': 'table table-bordered table-striped'}


class EventTable(tables.Table):
    name = tables.Column(verbose_name="Name", orderable=False)
    actions_list = tables.Column(
        empty_values=(),
        orderable=False,
        verbose_name="Actions"
        )
    trigger_list = tables.Column(
        empty_values=(),
        orderable=False,
        verbose_name="Triggers"
        )
    actions = tables.Column(
        empty_values=(),
        orderable=False,
        attrs={"th": {"style": "width:350px;"}},
        verbose_name=mark_safe("&nbsp;")
        )

    def render_name(self, record):
        url = reverse(
            "puzzle:event_update",
            kwargs={'puzzle_pk' : record.puzzle.pk, 'event_pk': record.pk}
            )
        return mark_safe("<img class='handle' src='" + static("images/handle.png") + "' /> " +
                         "<a href='" + url + "'>" + escape(record.name) + "</a>")

    def render_actions_list(self, record):
        return "Fill in with actions..."

    def render_trigger_list(self, record):
        return "Fill in with triggers..."

    def render_actions(self, record):
        item_kwargs = {'puzzle_pk':record.puzzle.pk, 'event_pk':record.pk}
        return action_buttons([
            ("Edit", reverse("puzzle:event_update", kwargs=item_kwargs)),
            ("Delete", reverse("puzzle:event_delete", kwargs=item_kwargs)),
            ("Triggers &amp; Actions", reverse("puzzle:event_manage", kwargs=item_kwargs))
            ])

    class Meta:
        template = "table.html"
        model = Event
        fields = ("name", "trigger_list", "actions_list", "actions")
        empty_text = "No events were found in puzzle."
        attrs = {'class': 'table table-bordered table-striped sortable'}
        row_attrs = {
            'data-id': lambda record: record.pk
            }


class TriggerTable(tables.Table):
    trigger_type = tables.Column(verbose_name="Type")
    params = tables.Column(
        empty_values=(),
        orderable=False,
        verbose_name="Params"
        )
    actions = tables.Column(
        empty_values=(),
        orderable=False,
        attrs={"th": {"style": "width:350px;"}},
        verbose_name=mark_safe("&nbsp;")
        )

    def render_params(self, record):
        return "Fill in with params..."

    def render_actions(self, record):
        item_kwargs = {'puzzle_pk':record.puzzle.pk, 'event_pk':record.pk}
        return action_buttons([
            ("Negate", reverse("puzzle:event_update", kwargs=item_kwargs)),
            ("Edit", reverse("puzzle:event_update", kwargs=item_kwargs)),
            ("Delete", reverse("puzzle:event_delete", kwargs=item_kwargs)),
            ])

    class Meta:
        template = "table.html"
        model = EventTrigger
        fields = ("trigger_type", "params", "actions")
        empty_text = "No triggers have been set on this event."
        attrs = {'class': 'table table-bordered table-striped'}


class ActionTable(tables.Table):
    action_type = tables.Column(verbose_name="Type")
    params = tables.Column(
        empty_values=(),
        orderable=False,
        verbose_name="Params"
        )
    actions = tables.Column(
        empty_values=(),
        orderable=False,
        attrs={"th": {"style": "width:350px;"}},
        verbose_name=mark_safe("&nbsp;")
        )

    def render_params(self, record):
        return "Fill in with params..."

    def render_actions(self, record):
        item_kwargs = {'puzzle_pk':record.puzzle.pk, 'event_pk':record.pk}
        return action_buttons([
            ("Edit", reverse("puzzle:event_update", kwargs=item_kwargs)),
            ("Delete", reverse("puzzle:event_delete", kwargs=item_kwargs)),
            ])

    class Meta:
        template = "table.html"
        model = EventAction
        fields = ("action_type", "params", "actions")
        empty_text = "No actions have been set on this event."
        attrs = {'class': 'table table-bordered table-striped'}
