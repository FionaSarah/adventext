from django.utils.safestring import mark_safe
from django import template
from django.core.urlresolvers import reverse, resolve
import json
from ..models import Room

register = template.Library()

@register.simple_tag
def nav_active(request, *urls):
    if resolve(request.path_info).url_name in urls:
        return "active"
    return ""

@register.filter(name='add_field_attrs')
def add_field_attrs(field, attributes):
    """Filter to add arbitrary html attributes to a field.
    Pass comma separated attribute names and their value
    separated with an =, e.g. class=test,foo=bar"""
    attributes = [x.split("=") for x in attributes.split(",")]
    attrs = {}
    for k,v in attributes:
        attrs[k] = v
    return field.as_widget(attrs=attrs)

@register.simple_tag
def item_location_translate(item_location):
    if item_location == -1:
        return "[ Player Inventory ]"
    elif item_location == 0:
        return "[ Nowhere ]"
    else:
        try:
            r = Room.objects.get(pk=item_location)
            return r.name
        except Room.DoesNotExist:
            return "?"

@register.simple_tag(takes_context = True)
def js_registry(context):
    """Spits out the current js_registry object as JSON for
    injecting in the page.
    See the JavascriptRegistry middleware."""
    return mark_safe("var registry = " + json.dumps(context['request'].js_registry)  + ";")
