from django.conf.urls import url

from . import views
from . import views_events
from ..play.views import BuildTestView, BuildTestRestartView

urlpatterns = [
    url(r'^$', views.PuzzleBuildList.as_view(), name='build'),
    url(r'^create$', views.PuzzleCreate.as_view(), name='create'),

    # Puzzles
    url(r'^(?P<puzzle_pk>[0-9]+)/$', views.PuzzleBuildIndex.as_view(), name='index'),
    url(r'^(?P<puzzle_pk>[0-9]+)/edit/$', views.PuzzleBuildEdit.as_view(), name='edit'),
    url(r'^(?P<puzzle_pk>[0-9]+)/delete/$', views.PuzzleBuildDelete.as_view(), name='delete'),

    # Rooms
    url(r'^(?P<puzzle_pk>[0-9]+)/rooms/$', views.PuzzleBuildRoomList.as_view(), name='room_list'),
    url(r'^(?P<puzzle_pk>[0-9]+)/rooms/add/$', views.PuzzleBuildRoomCreate.as_view(), name='room_create'),
    url(r'^(?P<puzzle_pk>[0-9]+)/rooms/edit/(?P<room_pk>[0-9]+)/$', views.PuzzleBuildRoomUpdate.as_view(), name='room_update'),
    url(r'^(?P<puzzle_pk>[0-9]+)/rooms/delete/(?P<room_pk>[0-9]+)/$', views.PuzzleBuildRoomDelete.as_view(), name='room_delete'),

    # Room directions
    url(r'^(?P<puzzle_pk>[0-9]+)/directions/(?P<room_pk>[0-9]+)/$', views.PuzzleBuildDirectionList.as_view(), name='direction_list'),
    url(r'^(?P<puzzle_pk>[0-9]+)/directions/(?P<room_pk>[0-9]+)/add/$', views.PuzzleBuildDirectionCreate.as_view(), name='direction_create'),
    url(r'^(?P<puzzle_pk>[0-9]+)/directions/(?P<room_pk>[0-9]+)/edit/(?P<direction_pk>[0-9]+)/$', views.PuzzleBuildDirectionUpdate.as_view(), name='direction_update'),
    url(r'^(?P<puzzle_pk>[0-9]+)/directions/(?P<room_pk>[0-9]+)/delete/(?P<direction_pk>[0-9]+)/$', views.PuzzleBuildDirectionDelete.as_view(), name='direction_delete'),

    # Items
    url(r'^(?P<puzzle_pk>[0-9]+)/items/$', views.PuzzleBuildItemList.as_view(), name='item_list'),
    url(r'^(?P<puzzle_pk>[0-9]+)/items/add/$', views.PuzzleBuildItemCreate.as_view(), name='item_create'),
    url(r'^(?P<puzzle_pk>[0-9]+)/items/edit/(?P<item_pk>[0-9]+)/$', views.PuzzleBuildItemUpdate.as_view(), name='item_update'),
    url(r'^(?P<puzzle_pk>[0-9]+)/items/delete/(?P<item_pk>[0-9]+)/$', views.PuzzleBuildItemDelete.as_view(), name='item_delete'),

    # Interactions
    url(r'^(?P<puzzle_pk>[0-9]+)/interactions/$', views.PuzzleBuildInteractionList.as_view(), name='interaction_list'),
    url(r'^(?P<puzzle_pk>[0-9]+)/interactions/add/$', views.PuzzleBuildInteractionCreate.as_view(), name='interaction_create'),
    url(r'^(?P<puzzle_pk>[0-9]+)/interactions/edit/(?P<interaction_pk>[0-9]+)/$', views.PuzzleBuildInteractionUpdate.as_view(), name='interaction_update'),
    url(r'^(?P<puzzle_pk>[0-9]+)/interaction/delete/(?P<interaction_pk>[0-9]+)/$', views.PuzzleBuildInteractionDelete.as_view(), name='interaction_delete'),

    # Events
    url(r'^(?P<puzzle_pk>[0-9]+)/events/$', views_events.PuzzleBuildEventList.as_view(), name='event_list'),
    url(r'^(?P<puzzle_pk>[0-9]+)/events/add/$', views_events.PuzzleBuildEventCreate.as_view(), name='event_create'),
    url(r'^(?P<puzzle_pk>[0-9]+)/events/edit/(?P<event_pk>[0-9]+)/$', views_events.PuzzleBuildEventUpdate.as_view(), name='event_update'),
    url(r'^(?P<puzzle_pk>[0-9]+)/events/delete/(?P<event_pk>[0-9]+)/$', views_events.PuzzleBuildEventDelete.as_view(), name='event_delete'),
    url(r'^(?P<puzzle_pk>[0-9]+)/events/reorder/$', views_events.PuzzleBuildEventReorder.as_view(), name='event_reorder'),

    url(r'^(?P<puzzle_pk>[0-9]+)/events/manage/(?P<event_pk>[0-9]+)/$', views_events.PuzzleBuildEventManage.as_view(), name='event_manage'),
    url(r'^(?P<puzzle_pk>[0-9]+)/events/manage/(?P<event_pk>[0-9]+)/trigger/add/$', views_events.PuzzleBuildEventTriggerCreate.as_view(), name='event_trigger_create'),

    url(r'^(?P<puzzle_pk>[0-9]+)/events/trigger_info/(?P<trigger_pk>[a-z_0-9]+)/$', views_events.PuzzleBuildEventTriggerInfo.as_view(), name='event_trigger_info'),

    # Testing
    url(r'^(?P<puzzle_pk>[0-9]+)/test/$', BuildTestView.as_view(), name='test'),
    url(r'^(?P<puzzle_pk>[0-9]+)/test/restart/$', BuildTestRestartView.as_view(), name='test_restart'),

    ]
