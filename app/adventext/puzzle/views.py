from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import ListView, FormView, CreateView, \
     DetailView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django_tables2 import SingleTableView

from .models import Puzzle, Room, Interaction, RoomDirection, Item, Interaction
from .forms import BuildPuzzleCreateForm, BuildPuzzleInfoForm, \
     BuildDirectionForm, BuildItemForm, BuildRoomForm, BuildInteractionForm
from .tables import PuzzleTable, RoomTable, RoomDirectionTable, ItemTable, \
     InteractionTable


# This is a quick decorator we use to check if a user is allowed into the build area
allowed_to_build_decorator = user_passes_test(
    lambda u: u.is_authenticated() and u.can_build(),
    login_url=reverse_lazy("home")
    )

class PuzzleBuildViewMixin:
    def get_puzzle_by_pk(self, pk):
        return Puzzle.objects.filter(pk=pk, author=self.request.user, approved=0)


##############################################################
#############         PUZZLE COMMON         ##################
##############################################################


class PuzzleCreate(FormView):
    """The form that we use to start creating a puzzle in the build interview"""
    template_name = "puzzle/create_form.html"
    form_class = BuildPuzzleCreateForm

    def form_valid(self, form):
        # Create new puzzle
        self.object = Puzzle()
        self.object.name = form.cleaned_data['name']
        self.object.author = self.request.user
        self.object.save()

        # Create empty room
        dummy_room = Room()
        dummy_room.puzzle = self.object
        dummy_room.name = "Empty Room"
        dummy_room.description = "You are in a boring empty room. You think the author should probably put something in here!"
        dummy_room.initial_message = "You enter a new room and are astounded to discover that there's literally nothing in it. How boring."
        dummy_room.save()

        # Update puzzle with room
        self.object.start_room = dummy_room
        self.object.save()

        # Create all initial interactions
        for short_name, long_name in Interaction.built_in_choices:
            new_interaction = Interaction()
            new_interaction.puzzle = self.object
            new_interaction.name = long_name
            new_interaction.built_in_short_name = short_name
            new_interaction.built_in = True
            new_interaction.save()

        messages.success(self.request, "New puzzle started.")

        return HttpResponseRedirect(
            reverse("puzzle:index", kwargs={'puzzle_pk':self.object.pk})
            )

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleCreate, self).dispatch(*args, **kwargs)


class PuzzleBuildList(SingleTableView):
    table_class = PuzzleTable

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Puzzle.objects.filter(author=self.request.user)


class PuzzleBuildIndex(DetailView, PuzzleBuildViewMixin):
    model = Puzzle
    template_name = "puzzle/build_index.html"
    context_object_name = "puzzle"
    pk_url_kwarg = "puzzle_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildIndex, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])


class PuzzleBuildEdit(UpdateView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_edit.html"
    model = Puzzle
    form_class = BuildPuzzleInfoForm
    pk_url_kwarg = "puzzle_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildEdit, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])

    def get_success_url(self):
        return reverse("puzzle:edit", kwargs={'puzzle_pk':self.object.pk})

    def form_valid(self, form):
        self.object = form.save()
        messages.success(self.request, "Puzzle info updated.")
        return HttpResponseRedirect(self.get_success_url())


class PuzzleBuildDelete(DeleteView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_delete.html"
    model = Puzzle
    pk_url_kwarg = "puzzle_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildDelete, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])

    def get_success_url(self):
        return reverse("puzzle:build")

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.delete()
        messages.success(self.request, "Puzzle deleted.")
        return HttpResponseRedirect(self.get_success_url())


##############################################################
#############              ROOMS            ##################
##############################################################


class PuzzleBuildRoomList(SingleTableView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_room_list.html"
    table_class = RoomTable

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildRoomList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return Room.objects.filter(puzzle=self.puzzle)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildRoomList, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


class PuzzleBuildRoomCreate(FormView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_room_create.html"
    form_class = BuildRoomForm

    def form_valid(self, form):
        self.object = Room()
        self.object.name = form.cleaned_data['name']
        self.object.description = form.cleaned_data['description']
        self.object.initial_message = form.cleaned_data['initial_message']
        self.object.puzzle = self.puzzle
        self.object.save()

        messages.success(self.request, "Room added.")

        return HttpResponseRedirect(
            reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk})
            )

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildRoomCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildRoomCreate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


class PuzzleBuildRoomUpdate(UpdateView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_room_update.html"
    model = Room
    form_class = BuildRoomForm
    pk_url_kwarg = "room_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildRoomUpdate, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Room.objects.filter(puzzle=self.puzzle, pk=self.kwargs['room_pk'])

    def get_success_url(self):
        return reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def form_valid(self, form):
        self.object = form.save()
        messages.success(self.request, "Room updated.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildRoomUpdate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


class PuzzleBuildRoomDelete(DeleteView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_room_delete.html"
    model = Room
    pk_url_kwarg = "room_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildRoomDelete, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Room.objects.filter(puzzle=self.puzzle, pk=self.kwargs['room_pk'])

    def get_success_url(self):
        return reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def delete(self, request, *args, **kwargs):
        room = self.get_object()
        if len(Room.objects.filter(puzzle=self.puzzle)) == 1:
            return error_out(request, "Puzzles must have at least one room.")
        if self.puzzle.start_room == room:
            return error_out(request, "Can't delete the puzzle start room.")
        if len(RoomDirection.objects.filter(destination=room)) > 0:
            return error_out(request, "There's at least one direction with this room as a destination. Delete them first.")
        # update all items in room to be nowhere
        for item in Item.objects.filter(location=room.pk):
            item.location = 0
            item.save()
        room.delete()
        messages.success(self.request, "Room deleted.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildRoomDelete, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def error_out(self, request, message):
        messages.warning(request, message)
        return HttpResponseRedirect(reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk}))


##############################################################
#############          DIRECTIONS           ##################
##############################################################


class PuzzleBuildDirectionList(SingleTableView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_direction_list.html"
    table_class = RoomDirectionTable

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        try:
            return super(PuzzleBuildDirectionList, self).dispatch(*args, **kwargs)
        except Room.DoesNotExist:
            messages.warning(self.request, "Room does not exist.")
            return HttpResponseRedirect(reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk}))

    def get_queryset(self):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        self.room = Room.objects.get(puzzle = self.puzzle, pk = self.kwargs['room_pk'])
        return RoomDirection.objects.filter(puzzle=self.puzzle, room=self.room)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildDirectionList, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        context['room'] = self.room
        return context


class PuzzleBuildDirectionCreate(FormView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_direction_create.html"
    model = RoomDirection
    form_class = BuildDirectionForm

    def form_valid(self, form):
        self.object = RoomDirection()
        self.object.room = self.room
        self.object.name = form.cleaned_data['name']
        if form.cleaned_data['manual_destination'] == "exit":
            self.object.destination = None
            self.object.is_exit = True
        else:
            self.object.destination = Room.objects.get(pk = int(form.cleaned_data['manual_destination']))
            self.object.is_exit = False
        self.object.initially_visible = form.cleaned_data['initially_visible']
        self.object.travel_message = form.cleaned_data['travel_message']
        self.object.puzzle = self.puzzle
        self.object.save()

        messages.success(self.request, "Room direction added.")

        return HttpResponseRedirect(
            reverse("puzzle:direction_list", kwargs={'puzzle_pk':self.puzzle.pk, 'room_pk':self.room.pk})
            )

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        try:
            self.room = Room.objects.get(puzzle = self.puzzle, pk = self.kwargs['room_pk'])
        except Room.DoesNotExist:
            messages.warning(self.request, "Room does not exist.")
            return HttpResponseRedirect(reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk}))
        return super(PuzzleBuildDirectionCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildDirectionCreate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        context['room'] = self.room
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildDirectionCreate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        kwargs['room'] = self.room
        return kwargs


class PuzzleBuildDirectionUpdate(UpdateView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_direction_update.html"
    model = RoomDirection
    pk_url_kwarg = "direction_pk"
    form_class = BuildDirectionForm

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        try:
            self.room = Room.objects.get(puzzle = self.puzzle, pk = self.kwargs['room_pk'])
        except Room.DoesNotExist:
            messages.warning(self.request, "Room does not exist.")
            return HttpResponseRedirect(reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk}))
        return super(PuzzleBuildDirectionUpdate, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return RoomDirection.objects.filter(puzzle=self.puzzle, room=self.room, pk=self.kwargs['direction_pk'])

    def get_success_url(self):
        return reverse("puzzle:direction_list", kwargs={'puzzle_pk':self.puzzle.pk, 'room_pk':self.room.pk})

    def form_valid(self, form):
        self.object = form.save()
        if form.cleaned_data['manual_destination'] == "exit":
            self.object.destination = None
            self.object.is_exit = True
        else:
            self.object.destination = Room.objects.get(pk = int(form.cleaned_data['manual_destination']))
            self.object.is_exit = False
        self.object.save()

        messages.success(self.request, "Room direction updated.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildDirectionUpdate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        context['room'] = self.room
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildDirectionUpdate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        kwargs['room'] = self.room
        return kwargs

    def get_initial(self):
        initial = self.initial.copy()
        initial['manual_destination'] = "exit" if self.object.is_exit else str(self.object.destination.pk)
        return initial


class PuzzleBuildDirectionDelete(DeleteView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_direction_delete.html"
    model = RoomDirection
    pk_url_kwarg = "direction_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        try:
            self.room = Room.objects.get(puzzle = self.puzzle, pk = self.kwargs['room_pk'])
        except Room.DoesNotExist:
            messages.warning(self.request, "Room does not exist.")
            return HttpResponseRedirect(reverse("puzzle:room_list", kwargs={'puzzle_pk':self.puzzle.pk}))
        return super(PuzzleBuildDirectionDelete, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return RoomDirection.objects.filter(puzzle=self.puzzle, room=self.room, pk=self.kwargs['direction_pk'])

    def get_success_url(self):
        return reverse("puzzle:direction_list", kwargs={'puzzle_pk':self.puzzle.pk, 'room_pk':self.room.pk})

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        messages.success(self.request, "Room direction deleted.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildDirectionDelete, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


##############################################################
#############              ITEMS            ##################
##############################################################


class PuzzleBuildItemList(SingleTableView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_item_list.html"
    table_class = ItemTable

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildItemList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return Item.objects.filter(puzzle=self.puzzle)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildItemList, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


class PuzzleBuildItemCreate(FormView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_item_create.html"
    form_class = BuildItemForm
    model = Item

    def form_valid(self, form):
        self.object = Item()
        self.object.name = form.cleaned_data['name']
        self.object.description = form.cleaned_data['description']
        self.object.is_exit = form.cleaned_data['is_exit']
        self.object.can_pickup = form.cleaned_data['can_pickup']
        self.object.visible = form.cleaned_data['visible']
        self.object.location = form.cleaned_data['manual_location']
        self.object.puzzle = self.puzzle
        self.object.save()

        messages.success(self.request, "Item added.")

        return HttpResponseRedirect(
            reverse("puzzle:item_list", kwargs={'puzzle_pk':self.puzzle.pk})
            )

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildItemCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildItemCreate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildItemCreate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        return kwargs


class PuzzleBuildItemUpdate(UpdateView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_item_update.html"
    form_class = BuildItemForm
    model = Item
    pk_url_kwarg = "item_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildItemUpdate, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Item.objects.filter(puzzle=self.puzzle, pk=self.kwargs['item_pk'])

    def get_success_url(self):
        return reverse("puzzle:item_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def form_valid(self, form):
        self.object.location = form.cleaned_data['manual_location']
        self.object = form.save()
        messages.success(self.request, "Item updated.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildItemUpdate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildItemUpdate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        return kwargs

    def get_initial(self):
        initial = self.initial.copy()
        initial['manual_location'] = self.object.location
        return initial


class PuzzleBuildItemDelete(DeleteView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_item_delete.html"
    model = Item
    pk_url_kwarg = "item_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildItemDelete, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Item.objects.filter(puzzle=self.puzzle, pk=self.kwargs['item_pk'])

    def get_success_url(self):
        return reverse("puzzle:item_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def delete(self, request, *args, **kwargs):
        item = self.get_object()
        item.delete()
        messages.success(self.request, "Item deleted.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildItemDelete, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


##############################################################
#############         INTERACTIONS          ##################
##############################################################


class PuzzleBuildInteractionList(SingleTableView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_interaction_list.html"
    table_class = InteractionTable

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildInteractionList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return Interaction.objects.filter(puzzle=self.puzzle).order_by('-built_in', 'pk')

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildInteractionList, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


class PuzzleBuildInteractionCreate(FormView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_interaction_create.html"
    form_class = BuildInteractionForm
    model = Interaction

    def form_valid(self, form):
        self.object = Interaction()
        self.object.name = form.cleaned_data['name']
        self.object.built_in = False
        self.object.initially_visible = form.cleaned_data['initially_visible']
        self.object.fail_message = form.cleaned_data['fail_message']
        self.object.puzzle = self.puzzle
        self.object.save()

        messages.success(self.request, "Interaction added.")

        return HttpResponseRedirect(
            reverse("puzzle:interaction_list", kwargs={'puzzle_pk':self.puzzle.pk})
            )

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildInteractionCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildInteractionCreate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildInteractionCreate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        return kwargs


class PuzzleBuildInteractionUpdate(UpdateView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_interaction_update.html"
    form_class = BuildInteractionForm
    model = Interaction
    pk_url_kwarg = "interaction_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildInteractionUpdate, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Interaction.objects.filter(puzzle=self.puzzle, pk=self.kwargs['interaction_pk'])

    def get_success_url(self):
        return reverse("puzzle:interaction_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def form_valid(self, form):
        self.object.name = form.cleaned_data['name']
        self.object.initially_visible = form.cleaned_data['initially_visible']
        if not self.object.built_in:
            self.object.fail_message = form.cleaned_data['fail_message']
        self.object = form.save()
        messages.success(self.request, "Interaction updated.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildInteractionUpdate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildInteractionUpdate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        return kwargs


class PuzzleBuildInteractionDelete(DeleteView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_interaction_delete.html"
    model = Interaction
    pk_url_kwarg = "interaction_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildInteractionDelete, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Interaction.objects.filter(puzzle=self.puzzle, pk=self.kwargs['interaction_pk'])

    def get_success_url(self):
        return reverse("puzzle:interaction_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def delete(self, request, *args, **kwargs):
        interaction = self.get_object()

        if interaction.built_in:
            messages.warning(request, "Cannot delete default interactions.")
            return HttpResponseRedirect(reverse("puzzle:interaction_list", kwargs={'puzzle_pk':self.puzzle.pk}))

        interaction.delete()
        messages.success(self.request, "Interaction deleted.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildInteractionDelete, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context
