"""
Events are a whole lotta stuff so they live in here to separate
from the main views file.
"""
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, \
     HttpResponseNotFound, JsonResponse
from django.views.generic import ListView, FormView, CreateView, \
     DetailView, UpdateView, DeleteView, View, TemplateView
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required, user_passes_test
from django.template import loader, Context
from django.contrib import messages
from django_tables2 import SingleTableView, MultiTableMixin

from .models import Event, EventTrigger, EventAction, ActionGroup, \
     ActionGroupAction, Variable
from .views import allowed_to_build_decorator, PuzzleBuildViewMixin
from .tables import EventTable, TriggerTable, ActionTable
from .forms import BuildEventForm, BuildEventTriggerForm
from adventext.play.quest import event_triggers


##############################################################
###############         EVENTS          ######################
##############################################################


class PuzzleBuildEventList(SingleTableView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_event/event_list.html"
    table_class = EventTable

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildEventList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        self.request.js_registry['puzzle_pk'] = self.puzzle.pk
        return Event.objects.filter(puzzle=self.puzzle).order_by('priority', 'pk')

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildEventList, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


class PuzzleBuildEventCreate(FormView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_event/event_create.html"
    form_class = BuildEventForm
    model = Event

    def form_valid(self, form):
        self.object = Event()
        self.object.name = form.cleaned_data['name']
        self.object.repeats = form.cleaned_data['repeats']
        self.object.puzzle = self.puzzle
        self.object.save()

        messages.success(self.request, "Event added.")

        return HttpResponseRedirect(
            reverse("puzzle:event_list", kwargs={'puzzle_pk':self.puzzle.pk})
            )

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildEventCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildEventCreate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildEventCreate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        return kwargs


class PuzzleBuildEventUpdate(UpdateView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_event/event_update.html"
    form_class = BuildEventForm
    model = Event
    pk_url_kwarg = "event_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildEventUpdate, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Event.objects.filter(puzzle=self.puzzle, pk=self.kwargs['event_pk'])

    def get_success_url(self):
        return reverse("puzzle:event_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def form_valid(self, form):
        self.object.name = form.cleaned_data['name']
        self.object.repeats = form.cleaned_data['repeats']
        self.object = form.save()
        messages.success(self.request, "Event updated.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildEventUpdate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildEventUpdate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        return kwargs


class PuzzleBuildEventDelete(DeleteView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_event/event_delete.html"
    model = Event
    pk_url_kwarg = "event_pk"

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildEventDelete, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Event.objects.filter(puzzle=self.puzzle, pk=self.kwargs['event_pk'])

    def get_success_url(self):
        return reverse("puzzle:event_list", kwargs={'puzzle_pk':self.puzzle.pk})

    def delete(self, request, *args, **kwargs):
        event = self.get_object()
        event.delete()
        messages.success(self.request, "Event deleted.")
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildEventDelete, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        return context


class PuzzleBuildEventReorder(View, PuzzleBuildViewMixin):
    http_method_names = ["post"]

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        return super(PuzzleBuildEventReorder, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        # This is fairly simple and stupid, but we should get posted a
        # bunch of event PKs along with their requested position, se
        # we might as well just blindly request them. Should be fine.
        # Famous last words.
        for event_pk in request.POST:
            try:
                event = Event.objects.get(puzzle=self.puzzle, pk=int(event_pk))
            except Event.DoesNotExist:
                continue
            event.priority = int(request.POST[event_pk])
            event.save()
        return HttpResponse()


class PuzzleBuildEventTriggerInfo(View, PuzzleBuildViewMixin):
    http_method_names = ["get"]
    form_class = BuildEventTriggerForm

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        self.trigger_pk = self.kwargs['trigger_pk']
        return super(PuzzleBuildEventTriggerInfo, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        if not self.trigger_pk in event_triggers:
            return HttpResponseNotFound()
        response = {}

        description_t = loader.get_template("puzzle/build_event/event_trigger_action_description.html")
        response['description'] = description_t.render(
            Context({
                'description': mark_safe(event_triggers[self.trigger_pk]['description'])
            }))

        form = self.form_class(puzzle = 0, event = 0)
        response['params'] = form.get_html_for_params(event_triggers[self.trigger_pk])

        return JsonResponse(response)


class PuzzleBuildEventManage(MultiTableMixin, PuzzleBuildViewMixin, TemplateView):
    template_name = "puzzle/build_event/event_manage.html"

    def get_tables(self):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        self.event = Event.objects.filter(pk=self.kwargs['event_pk'], puzzle=self.puzzle.pk)[0]
        self.request.js_registry['puzzle_pk'] = self.puzzle.pk
        trigger_q = EventTrigger.objects.filter(puzzle=self.puzzle)
        action_q = EventAction.objects.filter(puzzle=self.puzzle)
        self.tables = (
            TriggerTable(trigger_q),
            ActionTable(action_q)
            )
        return self.tables

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        return super(PuzzleBuildEventManage, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildEventManage, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        context['event'] = self.event
        return context


class PuzzleBuildEventTriggerCreate(FormView, PuzzleBuildViewMixin):
    template_name = "puzzle/build_event/event_trigger_create.html"
    model = EventTrigger
    form_class = BuildEventTriggerForm

    def form_valid(self, form):
        self.object = EventTrigger()
        self.object.puzzle = self.puzzle
        self.object.save()

        messages.success(self.request, "Event trigger added.")

        return HttpResponseRedirect(
            reverse("puzzle:event_manage", kwargs={'puzzle_pk':self.puzzle.pk, 'event_pk':self.room.pk})
            )

    @method_decorator(allowed_to_build_decorator)
    def dispatch(self, *args, **kwargs):
        self.puzzle = self.get_puzzle_by_pk(self.kwargs['puzzle_pk'])[0]
        self.request.js_registry['puzzle_pk'] = self.puzzle.pk
        try:
            self.event = Event.objects.get(puzzle = self.puzzle, pk = self.kwargs['event_pk'])
        except Room.DoesNotExist:
            messages.warning(self.request, "Event does not exist.")
            return HttpResponseRedirect(reverse("puzzle:event_list", kwargs={'puzzle_pk':self.puzzle.pk}))
        return super(PuzzleBuildEventTriggerCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PuzzleBuildEventTriggerCreate, self).get_context_data(**kwargs)
        context['puzzle'] = self.puzzle
        context['event'] = self.event
        return context

    def get_form_kwargs(self):
        kwargs = super(PuzzleBuildEventTriggerCreate, self).get_form_kwargs()
        kwargs['puzzle'] = self.puzzle
        kwargs['event'] = self.event
        return kwargs
