$(function(){

    // This is responsible for the reordering widget on the
    // event editing page. It uses jquery-sortable.
    var event_table = $('body#event_list table').sortable(
        {
            containerSelector: 'table',
            handle: 'img.handle',
            itemPath: '> tbody',
            itemSelector: 'tr',
            // This shows the "Drop to reorder" row when dragging. It looks a
            // bit weird with the two rows but it was seriously not playing ball
            // if I tried to colspan the single row. Probably some CSS fuckery
            // that I don't care to or want to understand
            placeholder: '<tr class="placeholder"/><tr class="placeholder_display"><td colspan="4">Drop event to reorder...</td></tr>',
            onDrop: function($item, container, _super, event)
            {
                _super($item, container);

                // Eeeerrrr so with the placeholder thing it wouldn't delete the ghosted
                // version of the row anymore. That's fine, I did it for it.
                $("tr.dragged").remove();

                // Gather the IDs in order. The way jquery-sortable was serialising these
                // was super unhelpful sooo, fuck it.
                var rows = {};
                var order = 0;
                $('body#event_list table tbody tr').each(
                    function()
                    {
                        rows[$(this).attr("data-id")] = ++order;
                    }
                );

                // Actually do the request
                $.ajax({
                    url: registry.base_url + "/puzzle/" + registry.puzzle_pk + "/events/reorder/",
                    type: 'post',
                    data: rows,
                    headers: {"X-CSRFToken": registry.csrf_token},
                    dataType: 'json',
                    success: function(data)
                    {
                        // Here's where one would put errors but lmao what do you take me for???
                        // I'm just happy this works at all.

                        // fucking javascript
                    }
                });
            }
        }
    );


    // When a trigger dropdown is changed, we need to
    // get the description and parameters
    $('body#event_trigger_create select#id_trigger_type').change(
        function()
        {

            var trigger_type = $(this).val();

            // Empty out if selected none
            if(trigger_type == '')
            {
                $("form div.description").empty();
                $("#div_id_params").empty();
                $("#div_id_secondary_params").empty();
                return;
            }

            // Show loading indicator
            $("form div.loading").show();

            // Ask server for the trigger info
            $.ajax({
                url: registry.base_url + "/puzzle/" + registry.puzzle_pk + "/events/trigger_info/" + trigger_type + "/",
                type: "get",
                headers: {"X-CSRFToken": registry.csrf_token},
                success: function(data)
                {
                    $("form div.description").empty().append(data.description);
                    $("form div.params").empty().append(data.params);
                    $("form div.loading").hide();
                }
            });

        }
    );

  });
