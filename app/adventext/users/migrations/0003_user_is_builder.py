# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20151025_1425'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_builder',
            field=models.BooleanField(default=False),
        ),
    ]
