# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_user_is_builder'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='is_builder',
            field=models.BooleanField(default=False, verbose_name='Allowed to create puzzles'),
        ),
    ]
